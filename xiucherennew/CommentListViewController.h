//
//  CommentListViewController.h
//  xiucherennew
//
//  Created by harry on 13-12-7.
//  Copyright (c) 2013年 harry. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Post.h"

@interface CommentListViewController : UIViewController

@property (weak, nonatomic) IBOutlet UIScrollView *sclMain;
@property (weak, nonatomic) IBOutlet UIImageView *imgLike;

- (IBAction)clickBack:(id)sender;
- (IBAction)clickResend:(id)sender;
- (IBAction)clickFeed:(id)sender;
- (IBAction)clickLike:(id)sender;

@property (strong, nonatomic) Post *post;
@end
