//
//  JobSendViewController.m
//  xiucherennew
//
//  Created by harry on 13-11-17.
//  Copyright (c) 2013年 harry. All rights reserved.
//

#import "JobSendViewController.h"

@interface JobSendViewController ()

@end

@implementation JobSendViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)clickCancel:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)clickSend:(id)sender {
    if ([_txtTitle.text length]==0 || [_txtContent.text length]==0) {
        [Utils simpleAlert:@"请完整内容后提交"];
        return ;
    }
    
    
    //NSJSONSerialization
    
    NSDictionary *dict = @{@"taskName": _txtTitle.text,@"publishPerson": [AppDelegate getAppDelegate].userCode,@"content": _txtContent.text};
    NSString *output = [dict JSONRepresentation];
    
    NSDictionary *dict2 = @{@"taskPerson":@""};
    NSString *output2 = [[NSArray arrayWithObjects:dict2,nil] JSONRepresentation];
    
                        
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    NSString *string = [NSString stringWithFormat:@"%@%@%@/%@/%@",WEBSERVICE_URL,WEBSERVICE_AddTask,[AppDelegate getAppDelegate].userCode,[output urlencode],[output2 urlencode]];
    
    [manager POST:string parameters:nil success:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        NSLog(@"JSON: %@", responseObject);
        
        NSString *result = [responseObject objectForKey:@"resultDataType"];
        
        NSLog(@"jsonArray: %@", result);
        
        //if([result isEqualToString:@"1"])
        {
           [Utils simpleAlert:@"提交成功" withDelegate:self];
        }
       
        
        [MBProgressHUD hideHUDForView:self.view animated:YES];
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"Error: %@", error);
        [MBProgressHUD hideHUDForView:self.view animated:YES];
    }];
    
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    [self.navigationController popViewControllerAnimated:YES];
}



@end
