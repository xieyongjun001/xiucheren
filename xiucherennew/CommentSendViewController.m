//
//  CommentSendViewController.m
//  xiucherennew
//
//  Created by harry on 13-12-7.
//  Copyright (c) 2013年 harry. All rights reserved.
//

#import "CommentSendViewController.h"

@interface CommentSendViewController ()

@end

@implementation CommentSendViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    [_txtContent becomeFirstResponder];
    _txtContent.placeholder = @"请填写评论";
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)clickCancel:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)clickSend:(id)sender {
    if ([_txtContent.text length]==0) {
        [Utils simpleAlert:@"请填写要评论的内容"];
        return ;
    }
    
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    NSString *string = [NSString stringWithFormat:@"%@%@%@/%@/%@",WEBSERVICE_URL,WEBSERVICE_SendPostComment,[AppDelegate getAppDelegate].userCode,_postGuid,[_txtContent.text urlencode]];
    NSLog(@"string: %@", string);
    
    [manager POST:string parameters:nil success:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        NSLog(@"JSON: %@", responseObject);
        
        NSString *result = [responseObject objectForKey:@"resultDataType"];
        
        NSLog(@"jsonArray: %@", result);
        
        if([result isEqualToString:@"1"]){
            [Utils simpleAlert:@"发送成功" withDelegate:self];
            
        }else{
            [Utils simpleAlert:@"发送失败"];
        }
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"Error: %@", error);
    }];
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    [self.navigationController popViewControllerAnimated:YES];
}
@end
