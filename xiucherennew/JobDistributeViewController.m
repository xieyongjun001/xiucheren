//
//  JobDistributeViewController.m
//  xiucherennew
//
//  Created by harry on 13-11-15.
//  Copyright (c) 2013年 harry. All rights reserved.
//

#import "JobDistributeViewController.h"
#import "MyJobsViewController.h"
#import "JobHomeViewController.h"

@interface JobDistributeViewController ()

@end

@implementation JobDistributeViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
}

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    
    NSDictionary *dis = [AppDelegate getAppDelegate].userDict;
    NSString *flag = [dis objectForKey:@"isPublishTask"];
    if ([flag isEqualToString:@"1"])
    {
        JobHomeViewController *myv = [[JobHomeViewController alloc] initWithNibName:@"JobHomeViewController" bundle:nil];
        [self.navigationController pushViewController:myv animated:NO];
    }else{
        MyJobsViewController *myv = [[MyJobsViewController alloc] initWithNibName:@"MyJobsViewController" bundle:nil];
        [self.navigationController pushViewController:myv animated:NO];
    }
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
