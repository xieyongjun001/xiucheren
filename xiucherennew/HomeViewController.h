//
//  HomeViewController.h
//  xiucherennew
//
//  Created by harry on 13-11-9.
//  Copyright (c) 2013年 harry. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface HomeViewController : UIViewController<UIAlertViewDelegate>

@property (weak, nonatomic) IBOutlet UITextField *txtAccount;
@property (weak, nonatomic) IBOutlet UITextField *txtPass;
- (IBAction)clickLogin:(id)sender;
- (IBAction)clickReg:(id)sender;
- (IBAction)clickForgetpass:(id)sender;
@end
