//
//  AppDelegate.h
//  xiucherennew
//
//  Created by harry on 13-11-9.
//  Copyright (c) 2013年 harry. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "LoadingViewController.h"
#import "HomeViewController.h"
#import "BPush.h"
@interface AppDelegate : UIResponder <UIApplicationDelegate,BPushDelegate>

@property (strong, nonatomic) UIWindow *window;
@property (strong, nonatomic) UIViewController *loadingvc;
@property (strong, nonatomic) NSString *userCode;
@property (strong, nonatomic) NSDictionary *userDict;
@property (strong, nonatomic) NSString *bduid;
@property (strong, nonatomic) NSString *bdcid;
+ (AppDelegate *)getAppDelegate;
@end
