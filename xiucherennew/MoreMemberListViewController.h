//
//  MoreMemberListViewController.h
//  xiucherennew
//
//  Created by harry on 13-11-16.
//  Copyright (c) 2013年 harry. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MoreMemberListViewController : UIViewController<UITableViewDataSource,UITableViewDelegate>
@property(nonatomic,assign)int gosixin;
@property(nonatomic,strong)NSMutableArray* selectedUser;
@property (weak, nonatomic) IBOutlet UITableView *tblview;
- (IBAction)clickBack:(id)sender;
@end
