//
//  KnowLedgeSubCatViewController.h
//  xiucherennew
//
//  Created by harry on 13-12-13.
//  Copyright (c) 2013年 harry. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface KnowLedgeSubCatViewController : UIViewController
@property (strong,nonatomic) NSString *typeguid;
@property (strong,nonatomic) NSString *typesname;
@property (weak, nonatomic) IBOutlet UITableView *tbview;
@property (weak, nonatomic) IBOutlet UILabel *lblTitle;
- (IBAction)clickBack:(id)sender;
@end
