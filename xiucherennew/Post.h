//
//  Post.h
//  xiucherennew
//
//  Created by harry on 13-11-13.
//  Copyright (c) 2013年 harry. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Post : NSObject
@property (nonatomic,strong)NSString *headerpath;
@property (nonatomic,strong)NSString *nickname;
@property (nonatomic,strong)NSString *date;
@property (nonatomic,strong)NSString *content;
@property (nonatomic,strong)NSString *postid;
@property (nonatomic,strong)NSString *huifunum;
@property (nonatomic,strong)NSString *zhfnum;
@property (nonatomic,strong)NSString *zannum;
@property (nonatomic,assign)int zanstatus;
@property (nonatomic,strong)NSString *imageContent;

@property (nonatomic,strong) NSString *title;
@end
