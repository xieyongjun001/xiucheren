//
//  MessageView.m
//  xiucherennew
//
//  Created by harry on 13-11-13.
//  Copyright (c) 2013年 harry. All rights reserved.
//

#import "MessageView.h"
#import "CommentSendViewController.h"
#import "CommentListViewController.h"
#import "ResentViewController.h"
#import "UIButton+WebCache.h"
#import <ShareSDK/ShareSDK.h>


@implementation MessageView

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}

- (id)initWithFrame:(CGRect)frame withData:(Post *)data type:(int)type{
    _type = type;
    return [self initWithFrame:frame withData:data];
}

- (id)initWithFrame:(CGRect)frame withData:(Post *)data{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
        self.backgroundColor = [UIColor whiteColor];
        
        _post = data;
        
        UIImageView *bgimageView = [[UIImageView alloc] initWithFrame:self.bounds];
        
        NSString *imgfix = @"messagebox_l_1";
        UIImage *img = [UIImage imageNamed:imgfix];
        [bgimageView setImage:img];
        [self addSubview:bgimageView];
        
        
        if (_type==0) {
            NSString *imgfix = @"messagebox_toolbar";
            UIImage *img = [UIImage imageNamed:imgfix];
            
            CGRect tmprect = self.bounds;
            tmprect.origin.y = self.bounds.size.height - img.size.height;
            tmprect.size.height = img.size.height;
            UIImageView *toolimageView = [[UIImageView alloc] initWithFrame:tmprect];
            [toolimageView setImage:img];
            [self addSubview:toolimageView];
        }
        
        
        UIImageView *headerImgv = [[UIImageView alloc] initWithFrame:CGRectMake(7, 8, 35, 35)];
        [headerImgv setImageWithURL:[NSURL URLWithString:data.headerpath]];
        [self addSubview:headerImgv];
        
        if ([data.nickname isEqual:[NSNull null]]) {
            UIImageView *imageView = [[UIImageView alloc] initWithFrame:CGRectMake(6, 6, 35, 35)];
            [imageView setImage:[UIImage imageNamed:@"header"]];
            [self addSubview:imageView];
        }else{
            UIImageView *imageView = [[UIImageView alloc] initWithFrame:CGRectMake(6, 6, 35, 35)];
            [imageView setImageWithURL:[NSURL URLWithString:data.headerpath] placeholderImage:nil options:SDWebImageRefreshCached];
            [self addSubview:imageView];
        }
        
        UILabel *titleLabel=[[UILabel alloc] initWithFrame:CGRectMake(50, 6, 200, 14)];
        titleLabel.backgroundColor=[UIColor clearColor];
        titleLabel.text=[data.nickname isEqual:[NSNull null]]?@"":data.nickname;
        titleLabel.font = [UIFont systemFontOfSize:12];
        titleLabel.textColor=[UIColor blackColor];
        titleLabel.textAlignment=NSTextAlignmentLeft;
        [self addSubview:titleLabel];
        
        UILabel *dateLabel=[[UILabel alloc] initWithFrame:CGRectMake(50, 29, 200, 14)];
        dateLabel.backgroundColor=[UIColor clearColor];
        dateLabel.text=[data.date isEqual:[NSNull null]]?@"":data.date;
        dateLabel.font = [UIFont systemFontOfSize:12];
        dateLabel.textColor=[UIColor blackColor];
        dateLabel.textAlignment=NSTextAlignmentLeft;
        [self addSubview:dateLabel];
        
        
        UILabel *typeLabel=[[UILabel alloc] initWithFrame:CGRectMake(250, 29, 60, 14)];
        typeLabel.backgroundColor=[UIColor clearColor];
        typeLabel.text=[data.title isEqual:[NSNull null]]?@"":data.title;
        typeLabel.font = [UIFont systemFontOfSize:12];
        typeLabel.textColor=[UIColor blackColor];
        typeLabel.textAlignment=NSTextAlignmentCenter;
        [self addSubview:typeLabel];
        
        float hh=self.bounds.size.height-MSGBOX_BOTTOMHEIGH-MSGBOX_TOPHEIGH;
        if (![_post.imageContent isEqualToString:@""]) {
            hh = self.bounds.size.height-MSGBOX_BOTTOMHEIGH-MSGBOX_TOPHEIGH-MSGBOX_IMGHEIGH;
        }
        
        UITextView *txtDesc = [[UITextView alloc] initWithFrame:CGRectMake(6, 50,290, hh)];
        txtDesc.backgroundColor = [UIColor clearColor];
        txtDesc.textColor = [UIColor blackColor];
        [txtDesc setFont:[UIFont systemFontOfSize:14]];
        txtDesc.editable = NO;
        txtDesc.text = [data.content isEqual:[NSNull null]]?@"":data.content;
        [self addSubview:txtDesc];
        
        float endy = txtDesc.frame.size.height+txtDesc.frame.origin.y+7;
        float h2 = self.bounds.size.height - MSGBOX_BOTTOMHEIGH;
         if (![_post.imageContent isEqualToString:@""]) {
             
             NSArray *arr = [_post.imageContent componentsSeparatedByString:@";"];
             if ([arr count]>0) {
                 
                 NSString *str = [arr objectAtIndex:0];
                 
                 UIImageView *imgv = [[UIImageView alloc] initWithFrame:CGRectMake(8,endy-32,290,180)];
                 [imgv setImageWithURL:[NSURL URLWithString:str]];
                 //imgv.backgroundColor = [UIColor redColor];
                 imgv.contentMode = UIViewContentModeScaleAspectFit;
                 [self addSubview:imgv];
                 
                 endy+= MSGBOX_IMGHEIGH;
             }
        }
        
        UIButton *centerbt = [[UIButton alloc] initWithFrame:CGRectMake(0,0,305,h2)];
        //centerbt.backgroundColor = [UIColor redColor];
        [centerbt addTarget:self action:@selector(centerClick) forControlEvents:UIControlEventTouchUpInside];
        [self addSubview:centerbt];
        
        if (_type!=1) {
           
            UIImage *leftimg=[UIImage imageNamed:@"simpleResent.png"];
            UIButton *leftbt = [[UIButton alloc] initWithFrame:CGRectMake(33,endy-2,leftimg.size.width/2,leftimg.size.height/2)];
            [leftbt setImage:leftimg forState:UIControlStateNormal];
            [leftbt addTarget:self action:@selector(leftClick) forControlEvents:UIControlEventTouchUpInside];
            [self addSubview:leftbt];
            
            UILabel *resentLabel=[[UILabel alloc] initWithFrame:CGRectMake(53, endy, 40, 14)];
            resentLabel.backgroundColor=[UIColor clearColor];
            resentLabel.text=[NSString stringWithFormat:@"%d",[data.zhfnum intValue]];
            resentLabel.font = [UIFont systemFontOfSize:12];
            resentLabel.textColor=[UIColor blackColor];
            resentLabel.textAlignment=NSTextAlignmentLeft;
            [self addSubview:resentLabel];
            
            UIButton *lbt = [[UIButton alloc] initWithFrame:CGRectMake(0,h2,100,32)];
            //lbt.backgroundColor = [UIColor redColor];
            [lbt addTarget:self action:@selector(leftClick) forControlEvents:UIControlEventTouchUpInside];
            [self addSubview:lbt];
            
            
            
            UIImage *rightimg=[UIImage imageNamed:@"simpleComment.png"];
            UIButton *rightbt = [[UIButton alloc] initWithFrame:CGRectMake(135,endy,rightimg.size.width/2,rightimg.size.height/2)];
            [rightbt setImage:rightimg forState:UIControlStateNormal];
            [rightbt addTarget:self action:@selector(rightClick) forControlEvents:UIControlEventTouchUpInside];
            [self addSubview:rightbt];
            
            UILabel *cmtLabel=[[UILabel alloc] initWithFrame:CGRectMake(155, endy, 40, 14)];
            cmtLabel.backgroundColor=[UIColor clearColor];
            cmtLabel.text=[NSString stringWithFormat:@"%d",[data.huifunum intValue]];
            cmtLabel.font = [UIFont systemFontOfSize:12];
            cmtLabel.textColor=[UIColor blackColor];
            cmtLabel.textAlignment=NSTextAlignmentLeft;
            [self addSubview:cmtLabel];
            
            UIButton *mbt = [[UIButton alloc] initWithFrame:CGRectMake(102,h2,100,32)];
            //mbt.backgroundColor = [UIColor redColor];
            [mbt addTarget:self action:@selector(rightClick) forControlEvents:UIControlEventTouchUpInside];
            [self addSubview:mbt];
            
            
            UIImage *likeimg=[UIImage imageNamed:@"simpleLike.png"];
            if (data.zanstatus==1) {
                likeimg=[UIImage imageNamed:@"simpleLikeRed.png"];
            }
            _likebt = [[UIButton alloc] initWithFrame:CGRectMake(235,endy,likeimg.size.width/2,likeimg.size.height/2)];
            [_likebt setImage:likeimg forState:UIControlStateNormal];
            if (data.zanstatus!=1) {
                [_likebt addTarget:self action:@selector(likeClick) forControlEvents:UIControlEventTouchUpInside];
            }else{
                [_likebt addTarget:self action:@selector(unlikeClick) forControlEvents:UIControlEventTouchUpInside];
            }
            [self addSubview:_likebt];
            
            _likeLabel=[[UILabel alloc] initWithFrame:CGRectMake(255, endy, 40, 14)];
            _likeLabel.backgroundColor=[UIColor clearColor];
            _likeLabel.text=[NSString stringWithFormat:@"%d",[data.zannum intValue]];
            _likeLabel.font = [UIFont systemFontOfSize:12];
            _likeLabel.textColor=[UIColor blackColor];
            _likeLabel.textAlignment=NSTextAlignmentLeft;
            [self addSubview:_likeLabel];
            
            _lkbt = [[UIButton alloc] initWithFrame:CGRectMake(204,h2,100,32)];
            //lkbt.backgroundColor = [UIColor redColor];
            if (data.zanstatus!=1) {
                [_lkbt addTarget:self action:@selector(likeClick) forControlEvents:UIControlEventTouchUpInside];
            }else{
                [_lkbt addTarget:self action:@selector(unlikeClick) forControlEvents:UIControlEventTouchUpInside];
            }
            [self addSubview:_lkbt];
        }
        
    }
    return self;
}

- (void) leftClick{
    NSString *imagePath = @"";
    if (![_post.imageContent isEqualToString:@""]) {
        NSArray *arr = [_post.imageContent componentsSeparatedByString:@";"];
        imagePath = [arr objectAtIndex:0];
    }

    
    //构造分享内容
    id<ISSContent> publishContent = [ShareSDK content:_post.content
                                       defaultContent:_post.content
                                                image:[ShareSDK imageWithUrl:imagePath]
                                                title:@"修车人"
                                                  url:[NSString stringWithFormat:@"%@%@",SHARE_WEIBO,_post.postid]
                                          description:_post.content
                                            mediaType:SSPublishContentMediaTypeNews];
    
    [ShareSDK showShareActionSheet:nil
                         shareList:nil
                           content:publishContent
                     statusBarTips:YES
                       authOptions:nil
                      shareOptions: nil
                            result:^(ShareType type, SSResponseState state, id<ISSPlatformShareInfo> statusInfo, id<ICMErrorInfo> error, BOOL end) {
                                if (state == SSResponseStateSuccess)
                                {
                                    NSLog(@"分享成功");
                                }
                                else if (state == SSResponseStateFail)
                                {
                                    NSLog(@"分享失败,错误码:%d,错误描述:%@", [error errorCode], [error errorDescription]);
                                }
                            }];
    //[Utils notOpen];
    
//    if([self.delegate respondsToSelector:@selector(didClickLeft:)])
//    {
//        [self.delegate didClickLeft:_post];
//    }
}

- (void) likeClick{
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    NSString *string = [NSString stringWithFormat:@"%@%@%@/%@",WEBSERVICE_URL,WEBSERVICE_PostZan,[AppDelegate getAppDelegate].userCode,_post.postid];
    
    [manager POST:string parameters:nil success:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        NSLog(@"JSON: %@", responseObject);
        
        UIImage *likeimg=[UIImage imageNamed:@"simpleLikeRed.png"];
        [_likebt setImage:likeimg forState:UIControlStateNormal];
        
        [_lkbt removeTarget:self action:@selector(likeClick) forControlEvents:UIControlEventTouchUpInside];
        [_lkbt addTarget:self action:@selector(unlikeClick) forControlEvents:UIControlEventTouchUpInside];
        
        int leb = [_likeLabel.text intValue]+1;
        _likeLabel.text = [NSString stringWithFormat:@"%d",leb];
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"Error: %@", error);
    }];

}

- (void) unlikeClick{
    int leb = [_likeLabel.text intValue]-1;
    if (leb>0) {
        AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
        NSString *string = [NSString stringWithFormat:@"%@%@%@/%@",WEBSERVICE_URL,WEBSERVICE_CancelZan,[AppDelegate getAppDelegate].userCode,_post.postid];
        
        [manager POST:string parameters:nil success:^(AFHTTPRequestOperation *operation, id responseObject) {
            
            NSLog(@"JSON: %@", responseObject);
            
            UIImage *likeimg=[UIImage imageNamed:@"simpleLike.png"];
            [_likebt setImage:likeimg forState:UIControlStateNormal];
            
            [_lkbt removeTarget:self action:@selector(unlikeClick) forControlEvents:UIControlEventTouchUpInside];
            [_lkbt addTarget:self action:@selector(likeClick) forControlEvents:UIControlEventTouchUpInside];
            
            int leb = [_likeLabel.text intValue]-1;
            _likeLabel.text = [NSString stringWithFormat:@"%d",leb];
            
        } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
            NSLog(@"Error: %@", error);
        }];
    }
}


- (void) rightClick{
    //[Utils notOpen];
    
    if([self.delegate respondsToSelector:@selector(didClickRight:)])
    {
        [self.delegate didClickRight:_post.postid];
    }
}

- (void) centerClick{
    //[Utils notOpen];
    
    if([self.delegate respondsToSelector:@selector(didClickCenter:)])
    {
        [self.delegate didClickCenter:_post];
    }
}
/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

@end
