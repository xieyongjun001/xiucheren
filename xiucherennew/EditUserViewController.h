//
//  EditUserViewController.h
//  xiucherennew
//
//  Created by harry on 13-11-27.
//  Copyright (c) 2013年 harry. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface EditUserViewController : UIViewController<UIAlertViewDelegate,UIPickerViewDelegate, UIPickerViewDataSource,UITextFieldDelegate>{
    UITextField * currentTextfield;//当前的文本筐
    BOOL keyboardIsShown;
    UITextField *txtloginname;
    UITextField *txtrealname;
    UITextField *txtage;
    UITextField *txtidno;
    UITextField *txtgender;
    UITextField *txtbirth;
    UITextField *txtblood;
    
    UITextField *txtqq;
    UITextField *txtmobile;
    UITextField *txtemail;
    UITextField *txtcity;
    UITextField *txtaddress;
    UITextField *txtzipcode;
    UITextField *txtschool;
    UITextField *txtworkexp1;
    UITextField *txtworkexp2;
}
@property (weak, nonatomic) IBOutlet UIButton *editbt;
@property (assign,nonatomic)int isedit;
@property (weak, nonatomic) IBOutlet UIImageView *imgvBg;
- (IBAction)clickBack:(id)sender;
- (IBAction)clickEdit:(id)sender;
@end
