//
//  CheckinPointViewController.h
//  xiucherennew
//
//  Created by harry on 13-11-21.
//  Copyright (c) 2013年 harry. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CheckinPointViewController : UIViewController

@property (weak, nonatomic) IBOutlet UIScrollView *sclview;
- (IBAction)clickBack:(id)sender;
@end
