//
//  CheckinPointViewController.m
//  xiucherennew
//
//  Created by harry on 13-11-21.
//  Copyright (c) 2013年 harry. All rights reserved.
//

#import "CheckinPointViewController.h"

@interface CheckinPointViewController ()
@property(nonatomic,strong)NSArray *pointarr;
@end

@implementation CheckinPointViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    NSString *string = [NSString stringWithFormat:@"%@%@%@",WEBSERVICE_URL,WEBSERVICE_CheckinList,[AppDelegate getAppDelegate].userCode];
    
    [manager GET:string parameters:nil success:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        NSLog(@"JSON: %@", responseObject);
        
        NSString *result = [responseObject objectForKey:@"resultDataType"];
        
        NSLog(@"jsonArray: %@", result);
        
        if([result isEqualToString:@"1"]){
            _pointarr = [responseObject objectForKey:@"resultList"];
           [self reloadPoints];
            
        }
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"Error: %@", error);
    }];
    
    
}



- (void)reloadPoints{
    for (int i=0; i<[_pointarr count]; i++) {
        NSDictionary *dic = [_pointarr objectAtIndex:i];
        
        UIImageView *imgv = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"checkin_box.png"]];
        imgv.frame = CGRectMake(0, i*26, 305, 26);
        [_sclview addSubview:imgv];
        
        UILabel *lblTitle = [[UILabel alloc] initWithFrame:CGRectMake(0, i*26+3,108,20)];
        lblTitle.font = [UIFont systemFontOfSize:14];
        lblTitle.textColor = [UIColor blackColor];
        lblTitle.backgroundColor = [UIColor clearColor];
        lblTitle.opaque = NO;
        lblTitle.textAlignment = NSTextAlignmentCenter;
        lblTitle.text = [dic objectForKey:@"auloPoint"];
        [_sclview addSubview:lblTitle];
        
        UILabel *lblPoint = [[UILabel alloc] initWithFrame:CGRectMake(110, i*26+3,195,20)];
        lblPoint.font = [UIFont systemFontOfSize:14];
        lblPoint.textColor = [UIColor blackColor];
        lblPoint.backgroundColor = [UIColor clearColor];
        lblPoint.opaque = NO;
        lblPoint.textAlignment = NSTextAlignmentCenter;
        lblPoint.text =[[dic objectForKey:@"auloLoginTime"] stringByReplacingOccurrencesOfString:@"00:00:00" withString:@""];
        [_sclview addSubview:lblPoint];
    }
    
    CGRect rect = _sclview.frame;
    _sclview.contentSize = CGSizeMake(rect.size.width, [_pointarr count]*(26+3));
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)clickBack:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}
@end
