//
//  MoreMobileViewController.m
//  xiucherennew
//
//  Created by harry on 13-11-12.
//  Copyright (c) 2013年 harry. All rights reserved.
//

#import "MoreMobileViewController.h"

@interface MoreMobileViewController ()

@end

@implementation MoreMobileViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    NSDictionary *dic = [AppDelegate getAppDelegate].userDict;
    _txtMobile.text = [dic objectForKey:@"auserMobile"];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)clickUpdate:(id)sender {
    
    
    if ([_txtMobile.text length]==0) {
        [Utils simpleAlert:@"请填写手机号码"];
        return ;
    }
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    NSString *string = [NSString stringWithFormat:@"%@%@%@/%@",WEBSERVICE_URL,WEBSERVICE_BindMobile,[AppDelegate getAppDelegate].userCode,_txtMobile.text];
    
    [manager POST:string parameters:nil success:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        NSLog(@"JSON: %@", responseObject);
        
        NSString *result = [responseObject objectForKey:@"resultDataType"];
        
        NSLog(@"jsonArray: %@", result);
        
        if([result isEqualToString:@"1"]){
            [Utils simpleAlert:@"绑定成功" withDelegate:self];
            
        }
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"Error: %@", error);
    }];
}


- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    
}

- (IBAction)clickBack:(id)sender {
     [self.navigationController popViewControllerAnimated:YES];
}
@end
