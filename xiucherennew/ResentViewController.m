//
//  ResentViewController.m
//  xiucherennew
//
//  Created by harry on 13-12-7.
//  Copyright (c) 2013年 harry. All rights reserved.
//

#import "ResentViewController.h"
#import "MoreMemberListViewController.h"

@interface ResentViewController ()
@property (nonatomic,strong) MoreMemberListViewController *mlvc;
@property (nonatomic,strong) UITextView *txtDesc;
@end

@implementation ResentViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    //[_txtContent becomeFirstResponder];
    _txtContent.placeholder = @"说说分享心得";
    
    UIView *containView = [[UIView alloc] initWithFrame:CGRectMake(8, 170, 305, 85)];
    containView.backgroundColor = [UIColor whiteColor];
    [self.view addSubview:containView];
    
    UIView *topline = [[UIView alloc] initWithFrame:CGRectMake(2, 0, 305, 1)];
    topline.backgroundColor = [UIColor darkGrayColor];
    [containView addSubview:topline];
    
    UIView *bottomline = [[UIView alloc] initWithFrame:CGRectMake(2, containView.bounds.size.height-1, 305, 1)];
    bottomline.backgroundColor = [UIColor darkGrayColor];
    [containView addSubview:bottomline];
    
    if ([_data.nickname isEqual:[NSNull null]]) {
        UIImageView *imageView = [[UIImageView alloc] initWithFrame:CGRectMake(2, 6, 72, 72)];
        [imageView setImage:[UIImage imageNamed:@"header"]];
        [containView addSubview:imageView];
    }else{
        UIImageView *imageView = [[UIImageView alloc] initWithFrame:CGRectMake(2, 6, 72, 72)];
        [imageView setImageWithURL:[NSURL URLWithString:_data.headerpath] placeholderImage:nil options:SDWebImageRefreshCached];
        [containView addSubview:imageView];
    }
    
    UILabel *titleLabel=[[UILabel alloc] initWithFrame:CGRectMake(85, 5, 200, 20)];
    titleLabel.backgroundColor=[UIColor clearColor];
    titleLabel.text=[@"@" stringByAppendingString:[_data.nickname isEqual:[NSNull null]]?@"":_data.nickname];
    titleLabel.font = [UIFont systemFontOfSize:18];
    titleLabel.textColor=[UIColor blackColor];
    titleLabel.textAlignment=NSTextAlignmentLeft;
    [containView addSubview:titleLabel];
    
    
    _txtDesc = [[UITextView alloc] initWithFrame:CGRectMake(85, 30,210, 50)];
    _txtDesc.backgroundColor = [UIColor clearColor];
    _txtDesc.textColor = [UIColor darkGrayColor];
    [_txtDesc setFont:[UIFont systemFontOfSize:14]];
    _txtDesc.editable = NO;
    _txtDesc.text = [_data.content isEqual:[NSNull null]]?@"":_data.content;
    [containView addSubview:_txtDesc];
}

- (void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    
    if (!_mlvc) {
        _mlvc = [[MoreMemberListViewController alloc] initWithNibName:@"MoreMemberListViewController" bundle:nil];
        _mlvc.gosixin = 3;
    }else{
        NSArray *arr = [NSArray arrayWithArray:_mlvc.selectedUser];
        NSLog(@"%@",arr);
        NSString *dd = @"";
        for (int i=0; i<[arr count]; i++) {
            NSDictionary *udict = [arr objectAtIndex:i];
            NSString *uname = [udict objectForKey:@"auserName"];
            NSString *tm = [@"@" stringByAppendingString:uname];
            _txtContent.text = [_txtContent.text stringByReplacingOccurrencesOfString:tm withString:@""];
            
            dd = [dd stringByAppendingFormat:@"@%@ ",uname];
        }
        _txtContent.text = [NSString stringWithFormat:@"%@%@",dd,_txtContent.text];

    }
    
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
    
}

- (IBAction)clickCancel:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)clickSend:(id)sender {
//    if ([_txtContent.text length]==0) {
//        [Utils simpleAlert:@"请填写要评论的内容"];
//        return ;
//    }
    
    NSDictionary *parameters = @{@"userCode": [AppDelegate getAppDelegate].userCode,@"postGuid": _data.postid,@"tieContent": _txtContent.text};
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    NSString *string = [NSString stringWithFormat:@"%@%@%@/message/%@/%@",WEBSERVICE_URL,WEBSERVICE_SendFeed,[AppDelegate getAppDelegate].userCode,_data.postid,[_txtContent.text urlencode]];
    NSLog(@"string: %@", string);
    
    [manager POST:string parameters:parameters success:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        NSLog(@"JSON: %@", responseObject);
        
        NSString *result = [responseObject objectForKey:@"resultDataType"];
        
        NSLog(@"jsonArray: %@", result);
        
        if([result isEqualToString:@"1"]){
            [Utils simpleAlert:@"发送成功" withDelegate:self];
            
        }else{
            [Utils simpleAlert:@"发送失败"];
        }
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"Error: %@", error);
    }];
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)clickAt:(id)sender {
    [self.navigationController pushViewController:_mlvc animated:YES];

}
@end
