//
//  MoreMemberDetailViewController.h
//  xiucherennew
//
//  Created by harry on 13-11-16.
//  Copyright (c) 2013年 harry. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Member.h"
@interface MoreMemberDetailViewController : UIViewController
@property (strong, nonatomic) Member *mb;
@property (weak, nonatomic) IBOutlet UIImageView *imgvHeader;
@property (weak, nonatomic) IBOutlet UILabel *lblName;
@property (weak, nonatomic) IBOutlet UILabel *lblAge;
@property (weak, nonatomic) IBOutlet UILabel *lblGender;
@property (weak, nonatomic) IBOutlet UILabel *lblMobile;
@property (weak, nonatomic) IBOutlet UILabel *lblMail;
@property (weak, nonatomic) IBOutlet UILabel *lblStationName;
@property (weak, nonatomic) IBOutlet UILabel *lblStationCode;
- (IBAction)clickBack:(id)sender;
@end
