//
//  MoreMailViewController.m
//  xiucherennew
//
//  Created by harry on 13-11-12.
//  Copyright (c) 2013年 harry. All rights reserved.
//

#import "MoreMailViewController.h"

@interface MoreMailViewController ()

@end

@implementation MoreMailViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    NSDictionary *dic = [AppDelegate getAppDelegate].userDict;
    _txtMail.text = [dic objectForKey:@"auserMail"];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)clickBack:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)clickUpdate:(id)sender {
    
    if ([_txtMail.text length]==0) {
        [Utils simpleAlert:@"请填写邮箱"];
        return ;
    }
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    NSString *string = [NSString stringWithFormat:@"%@%@%@/%@",WEBSERVICE_URL,WEBSERVICE_BindMail,[AppDelegate getAppDelegate].userCode,_txtMail.text];
    
    [manager POST:string parameters:nil success:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        NSLog(@"JSON: %@", responseObject);
        
        NSString *result = [responseObject objectForKey:@"resultDataType"];
        
        NSLog(@"jsonArray: %@", result);
        
        if([result isEqualToString:@"1"]){
            [Utils simpleAlert:@"绑定成功" withDelegate:self];
            
        }
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"Error: %@", error);
    }];
    
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    [self.navigationController popViewControllerAnimated:YES];
}

@end
