//
//  MyJobsViewController.m
//  xiucherennew
//
//  Created by harry on 13-11-15.
//  Copyright (c) 2013年 harry. All rights reserved.
//

#import "MyJobsViewController.h"
#import "MyJobDetailViewController.h"

@interface MyJobsViewController ()
@property (strong, nonatomic)NSMutableArray *joblist;

@end

@implementation MyJobsViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
     [_tableview setAllowsSelection:NO];
    
    NSArray *arr = [self.navigationController viewControllers];
    NSArray *arr2 = [[NSArray alloc] initWithObjects:[arr objectAtIndex:1], nil];
    [self.navigationController setViewControllers:arr2];
    
     _joblist = [[NSMutableArray alloc] init];
    
    
   
    /*
    NSArray *arr = [NSArray arrayWithObjects:@"任务1", @"任务2", @"任务3", @"任务4", nil];
    NSDictionary *dic = [[NSDictionary alloc] initWithObjects:[NSArray arrayWithObjects:@"2013-11-15",arr,nil] forKeys:[NSArray arrayWithObjects:@"date",@"job",nil]];
    
    NSArray *arr2 = [NSArray arrayWithObjects:@"任务a", @"任务b", @"任务c", nil];
    NSDictionary *dic2 = [[NSDictionary alloc] initWithObjects:[NSArray arrayWithObjects:@"2013-11-18",arr2,nil] forKeys:[NSArray arrayWithObjects:@"date",@"job",nil]];
    
    _joblist = [NSArray arrayWithObjects:dic,dic2, nil];
     */
}

- (void)viewDidAppear:(BOOL)animated{
     [self reloadList];
}


- (void)reloadList{
    
    [_joblist removeAllObjects];
    
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    NSString *string = [NSString stringWithFormat:@"%@%@%@",WEBSERVICE_URL,WEBSERVICE_MyReceavedJob,[AppDelegate getAppDelegate].userCode];
    
    [manager GET:string parameters:nil success:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        NSLog(@"JSON: %@", responseObject);
        
        NSString *result = [responseObject objectForKey:@"resultDataType"];
        
        NSLog(@"jsonArray: %@", result);
        
        if([result isEqualToString:@"1"]){
            //_joblist = [responseObject objectForKey:@"resultList"];
            
            
            NSArray *arr = [responseObject objectForKey:@"resultList"];
            
            
            NSArray *sortedArray = [arr sortedArrayUsingComparator:^(id obj1, id obj2){
                NSDate *date1 = [Utils NSStringDateToNSDate:[obj1 objectForKey:@"publishTime"] withFormatter:@"yyyy-mm-dd HH:mm:ss"];
                NSDate *date2 = [Utils NSStringDateToNSDate:[obj2 objectForKey:@"publishTime"] withFormatter:@"yyyy-mm-dd HH:mm:ss"];
                return [date1 compare:date2];
            }];
            
            //NSMutableSet *dateset = [[NSMutableSet alloc] init];
            NSMutableArray *dateset = [[NSMutableArray alloc] init];
            for (int i=0; i<[arr count]; i++) {
                NSDictionary *dic = [arr objectAtIndex:i];
                NSString *dic2 = [dic objectForKey:@"publishTime"];
                /*
                NSString *datestring = [NSString stringWithFormat:@"%@-%@-%@",[dic2 objectForKey:@"year"],[dic2 objectForKey:@"month"],[dic2 objectForKey:@"date"]];
                 */
                NSArray *t = [dic2 componentsSeparatedByString:@" "];
                NSString *datestring = [t objectAtIndex:0];
                if ([dateset indexOfObject:datestring]==NSNotFound) {
                    [dateset addObject:datestring];
                }
            }
            
            
            for (NSString *string in dateset)
            {
                NSMutableArray *jobarr = [[NSMutableArray alloc] init];
                for (int i=0; i<[arr count]; i++) {
                    NSDictionary *dic = [arr objectAtIndex:i];
                    NSString *dic2 = [dic objectForKey:@"publishTime"];
                    NSArray *t = [dic2 componentsSeparatedByString:@" "];
                    NSString *datestring = [t objectAtIndex:0];
                    if ([string isEqualToString:datestring]) {
                        [jobarr addObject:dic];
                    }
                }
                NSDictionary *resdic = @{@"date" :string, @"job" :jobarr};
                [_joblist addObject:resdic];
            }
        }
        [_tableview reloadData];
        
        [MBProgressHUD hideHUDForView:self.view animated:YES];
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"Error: %@", error);
        [MBProgressHUD hideHUDForView:self.view animated:YES];
    }];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}



#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    return [_joblist count];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    // Return the number of rows in the section.
    NSDictionary *dic = [_joblist objectAtIndex:section];
    NSArray *arr = [dic objectForKey:@"job"];
    return [arr count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
        
        UIImage *img=[UIImage imageNamed:@"myjob_complete.png"];
        UIButton *bt = [[UIButton alloc] initWithFrame:CGRectMake(15,19,img.size.width/2,img.size.height/2)];
        [bt addTarget:self action:@selector(statusClick:) forControlEvents:UIControlEventTouchUpInside];
        [cell.contentView addSubview:bt];
        
        UILabel *lblTitle = [[UILabel alloc] initWithFrame:CGRectMake(70, 21,200,16)];
        lblTitle.font = [UIFont boldSystemFontOfSize:16];
        lblTitle.backgroundColor = [UIColor clearColor];
        lblTitle.textColor = [Utils colorWithHexString:@"009943"];
        lblTitle.opaque = NO;
        lblTitle.textAlignment = NSTextAlignmentLeft;
        [cell.contentView addSubview:lblTitle];
        
        
        UIImage *img2=[UIImage imageNamed:@"viewdetail.png"];
        UIButton *bt2 = [[UIButton alloc] initWithFrame:CGRectMake(cell.frame.size.width-img2.size.width-10,
                                                                   (cell.frame.size.height-img2.size.height)/2,
                                                                   img2.size.width,img2.size.height)];
        [bt2 addTarget:self action:@selector(detailClick:) forControlEvents:UIControlEventTouchUpInside];
        [cell.contentView addSubview:bt2];

    }
    NSDictionary *dic = [_joblist objectAtIndex:indexPath.section];
    NSArray *arr = [dic objectForKey:@"job"];
    NSDictionary *dic2 = [arr objectAtIndex:indexPath.row];
    int status = [[dic2 objectForKey:@"taskStatus"] intValue];
    
    int count = [cell.contentView.subviews count];
    UIButton *bt = (UIButton *)[cell.contentView.subviews objectAtIndex:count - 3];
    UILabel *lblTitle = (UILabel *)[cell.contentView.subviews objectAtIndex:count - 2];
    UIButton *bt2 = (UIButton *)[cell.contentView.subviews objectAtIndex:count - 1];
    
    UIImage *img=[UIImage imageNamed:@"myjob_complete.png"];
    if (status==0) {
        img=[UIImage imageNamed:@"myjob_goingon"];
        lblTitle.textColor = [Utils colorWithHexString:@"e50011"];
    }else{
        img=[UIImage imageNamed:@"myjob_complete.png"];
        lblTitle.textColor = [Utils colorWithHexString:@"009943"];
    }
    [bt setImage:img forState:UIControlStateNormal];
    bt.tag = indexPath.section*100+indexPath.row;
    bt.frame = CGRectMake(15,19,img.size.width/2,img.size.height/2);
    
    UIImage *img2=[UIImage imageNamed:@"viewdetail.png"];
    [bt2 setImage:img2 forState:UIControlStateNormal];
    bt2.tag = indexPath.section*100+indexPath.row;
    bt2.frame = CGRectMake(cell.frame.size.width-img2.size.width/2,1,img2.size.width/2,img2.size.height/2);
    
    lblTitle.tag = 2000+indexPath.section*100+indexPath.row;
    lblTitle.text = [dic2 objectForKey:@"taskName"];
    
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 60;
}

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section{
    NSDictionary *dic = [_joblist objectAtIndex:section];
    NSString *arr = [dic objectForKey:@"date"];

    return arr;
}

/*
 // Override to support conditional editing of the table view.
 - (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
 {
 // Return NO if you do not want the specified item to be editable.
 return YES;
 }
 */

/*
 // Override to support editing the table view.
 - (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
 {
 if (editingStyle == UITableViewCellEditingStyleDelete) {
 // Delete the row from the data source
 [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
 }
 else if (editingStyle == UITableViewCellEditingStyleInsert) {
 // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
 }
 }
 */

/*
 // Override to support rearranging the table view.
 - (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath
 {
 }
 */

/*
 // Override to support conditional rearranging of the table view.
 - (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath
 {
 // Return NO if you do not want the item to be re-orderable.
 return YES;
 }
 */


#pragma mark - Table view delegate

// In a xib-based application, navigation from a table can be handled in -tableView:didSelectRowAtIndexPath:
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Navigation logic may go here, for example:
    // Create the next view controller.
//    MyJobDetailViewController *detailViewController = [[MyJobDetailViewController alloc] initWithNibName:@"MyJobDetailViewController" bundle:nil];
//    
//    // Pass the selected object to the new view controller.
//    
//    
//    NSDictionary *dic = [_joblist objectAtIndex:indexPath.section];
//    NSArray *arr = [dic objectForKey:@"job"];
//    NSDictionary *dic2 = [arr objectAtIndex:indexPath.row];
//    NSString *taskGuid = [dic2 objectForKey:@"taskGuid"];
//    
//    detailViewController.taskGuid = taskGuid;
//    // Push the view controller.
//    [self.navigationController pushViewController:detailViewController animated:YES];
}


- (void)detailClick:(UIButton *)bt{
    int tagid = bt.tag;
    int section = tagid/100;
    int row = tagid-100*section;
    
    MyJobDetailViewController *detailViewController = [[MyJobDetailViewController alloc] initWithNibName:@"MyJobDetailViewController" bundle:nil];
    
    // Pass the selected object to the new view controller.
    
    
    NSDictionary *dic = [_joblist objectAtIndex:section];
    NSArray *arr = [dic objectForKey:@"job"];
    NSDictionary *dic2 = [arr objectAtIndex:row];
    NSString *taskGuid = [dic2 objectForKey:@"taskGuid"];
    
    detailViewController.taskGuid = taskGuid;
    // Push the view controller.
    [self.navigationController pushViewController:detailViewController animated:YES];
}


- (void)statusClick:(UIButton *)bt{
    

    int tagid = bt.tag;
    int section = tagid/100;
    int row = tagid-100*section;
    
    NSDictionary *dic = [_joblist objectAtIndex:section];
    NSArray *arr = [dic objectForKey:@"job"];
    NSDictionary *dic2 = [arr objectAtIndex:row];
    NSString *taskGuid = [dic2 objectForKey:@"taskGuid"];
    NSString *rowGuid = [dic2 objectForKey:@"rowGuid"];
    NSString *content = [dic2 objectForKey:@"content"];
    int status = [[dic2 objectForKey:@"taskStatus"] intValue];
    
    if (status==0) {
        status=1;
    }else{
        status = 0;
    }
    NSString *statusstring = [NSString stringWithFormat:@"%d",status];

    
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    
    NSDictionary *dict = @{@"rowGuid": rowGuid,@"taskGuid":taskGuid,@"taskPerson": [AppDelegate getAppDelegate].userCode,@"content": content,@"taskStatus": statusstring};
    NSString *output = [dict JSONRepresentation];
    
    NSString *string = [NSString stringWithFormat:@"%@%@%@/%@",WEBSERVICE_URL,WEBSERVICE_UpdateTask,[AppDelegate getAppDelegate].userCode,[output urlencode]];
    
    [manager POST:string parameters:nil success:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        NSLog(@"JSON: %@", responseObject);
        
        NSString *result = [responseObject objectForKey:@"resultDataType"];
        
        NSLog(@"jsonArray: %@", result);
        
        [MBProgressHUD hideHUDForView:self.view animated:YES];
        
        if([result isEqualToString:@"1"]){
            //_joblist = [responseObject objectForKey:@"resultList"];
            [self reloadList];
        }
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"Error: %@", error);
        [MBProgressHUD hideHUDForView:self.view animated:YES];
    }];

    
    UIImage *img=[UIImage imageNamed:@"myjob_goingon"];
    [bt setImage:img forState:UIControlStateNormal];
    bt.frame = CGRectMake(15,15,img.size.width/2,img.size.height/2);
    
    UIView *cell = bt.superview;
    NSArray *subview = [cell subviews];

    
    for (int i=0;i<[subview count];i++) {
        if ([[subview objectAtIndex:i] isKindOfClass:[UILabel class]]) {
            UILabel *lbl = [subview objectAtIndex:i];
            lbl.textColor = [Utils colorWithHexString:@"e50011"];
        }
    }

}
@end
