//
//  MyJobsViewController.h
//  xiucherennew
//
//  Created by harry on 13-11-15.
//  Copyright (c) 2013年 harry. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MyJobsViewController : UIViewController

@property (weak, nonatomic) IBOutlet UITableView *tableview;
@end
