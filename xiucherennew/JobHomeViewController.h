//
//  JobHomeViewController.h
//  xiucherennew
//
//  Created by harry on 13-11-16.
//  Copyright (c) 2013年 harry. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface JobHomeViewController : UIViewController<UITableViewDataSource,UITableViewDelegate>

@property (weak, nonatomic) IBOutlet UITableView *tbview;
- (IBAction)clickMan:(id)sender;
- (IBAction)clickEdit:(id)sender;
@end
