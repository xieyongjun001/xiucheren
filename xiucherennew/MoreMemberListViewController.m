//
//  MoreMemberListViewController.m
//  xiucherennew
//
//  Created by harry on 13-11-16.
//  Copyright (c) 2013年 harry. All rights reserved.
//

#import "MoreMemberListViewController.h"
#import "MoreMemberDetailViewController.h"
#import "MoreSixinViewController.h"
#import "Member.h"

@interface MoreMemberListViewController ()
@property (strong, nonatomic)NSMutableArray *mblist;

@end

@implementation MoreMemberListViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    _mblist = [[NSMutableArray alloc] initWithObjects: nil];
    _selectedUser = [NSMutableArray arrayWithObjects: nil];
    
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    NSDictionary *dic = [AppDelegate getAppDelegate].userDict;
    NSString *string = [NSString stringWithFormat:@"%@%@%@",WEBSERVICE_URL,WEBSERVICE_GetStaff,[dic objectForKey:@"auorgCode"]];
    
    [manager GET:string parameters:nil success:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        NSLog(@"JSON: %@", responseObject);
        
        NSString *result = [responseObject objectForKey:@"resultDataType"];
        
        NSLog(@"jsonArray: %@", result);
        
        if([result isEqualToString:@"1"]){
            NSArray *arr = [responseObject objectForKey:@"resultList"];
            if([arr count]>0){
                NSDictionary *dic = [arr objectAtIndex:0];
                [_mblist addObjectsFromArray:[dic objectForKey:@"oaUserInfos"]];
                if (_gosixin==2) {
                    int needremove = -1;
                    for (int i=0; i<[_mblist count]; i++) {
                        NSDictionary *dd = [_mblist objectAtIndex:i];
                        NSLog(@"%@--%@",[dd objectForKey:@"auserCode"],[AppDelegate getAppDelegate].userCode);
                        if ([[dd objectForKey:@"auserCode"] isEqualToString:[AppDelegate getAppDelegate].userCode]) {
                            needremove=i;
                            break;
                        }
                    }
                    if (needremove>-1) {
                        [_mblist removeObjectAtIndex:needremove];
                    }
                }
                [_tblview reloadData];
            }
        }

        
        [MBProgressHUD hideHUDForView:self.view animated:YES];
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"Error: %@", error);
        [MBProgressHUD hideHUDForView:self.view animated:YES];
    }];

}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    // Return the number of rows in the section.
    return [_mblist count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
    }
    
    
    cell.selectionStyle = UITableViewCellSelectionStyleNone;

    
    NSDictionary *dic = [_mblist objectAtIndex:indexPath.row];
    
    UILabel *lblTitle = [[UILabel alloc] initWithFrame:CGRectMake(70, 21,200,16)];
    lblTitle.font = [UIFont systemFontOfSize:16];
    lblTitle.textColor = [UIColor blackColor];
    lblTitle.backgroundColor = [UIColor clearColor];
    lblTitle.opaque = NO;
    lblTitle.textAlignment = NSTextAlignmentLeft;
    
    lblTitle.text = [dic objectForKey:@"auserName"];
    [cell.contentView addSubview:lblTitle];
    
    UIImage *bgimg = [UIImage imageNamed:@"header.png"];
    UIImageView *bgimgv = [[UIImageView alloc] initWithFrame:CGRectMake(15, 15, 30,30)];
    bgimgv.contentMode = UIViewContentModeScaleAspectFill;
    if (![[dic objectForKey:@"userHeadImagePath"] isEqualToString:@""]) {
        [bgimgv setImageWithURL:[NSURL URLWithString:[dic objectForKey:@"userHeadImagePath"]] placeholderImage:nil];
    }else{
        [bgimgv setImage:bgimg];
    }
    
    [cell.contentView addSubview:bgimgv];
    
    return cell;
}

/*
 // Override to support conditional editing of the table view.
 - (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
 {
 // Return NO if you do not want the specified item to be editable.
 return YES;
 }
 */

/*
 // Override to support editing the table view.
 - (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
 {
 if (editingStyle == UITableViewCellEditingStyleDelete) {
 // Delete the row from the data source
 [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
 }
 else if (editingStyle == UITableViewCellEditingStyleInsert) {
 // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
 }
 }
 */

/*
 // Override to support rearranging the table view.
 - (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath
 {
 }
 */

/*
 // Override to support conditional rearranging of the table view.
 - (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath
 {
 // Return NO if you do not want the item to be re-orderable.
 return YES;
 }
 */

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 60;
}


- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Navigation logic may go here, for example:
    // Create the next view controller.
    if (_gosixin==1) {
        
        MoreMemberDetailViewController *detailViewController = [[MoreMemberDetailViewController alloc] initWithNibName:@"MoreMemberDetailViewController" bundle:nil];
        detailViewController.mb = [[Member alloc] initWithDic:[_mblist objectAtIndex:indexPath.row]];
        [self.navigationController pushViewController:detailViewController animated:YES];
    }
    if (_gosixin==2) {
        NSDictionary *udict = [_mblist objectAtIndex:indexPath.row];
        MoreSixinViewController *detailViewController = [[MoreSixinViewController alloc] initWithNibName:@"MoreSixinViewController" bundle:nil];
        detailViewController.nickname = [udict objectForKey:@"auserName"];
        detailViewController.usercode = [udict objectForKey:@"auserCode"];
        detailViewController.headimg = [udict objectForKey:@"userHeadImagePath"];
        [self.navigationController pushViewController:detailViewController animated:YES];
    }
    if (_gosixin==3) {
        NSString *mbname = [_mblist objectAtIndex:indexPath.row];
        UITableViewCell *cell = [tableView cellForRowAtIndexPath:indexPath];
        if(cell.accessoryType != UITableViewCellAccessoryCheckmark){
            cell.accessoryType = UITableViewCellAccessoryCheckmark;
            [_selectedUser addObject:mbname];
        }else{
             cell.accessoryType = UITableViewCellAccessoryNone;
            [_selectedUser removeObject:mbname];
        }
    }
}

/*
 #pragma mark - Navigation
 
 // In a story board-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
 {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 
 */

- (IBAction)clickBack:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];

}
@end
