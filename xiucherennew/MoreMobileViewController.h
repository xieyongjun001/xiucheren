//
//  MoreMobileViewController.h
//  xiucherennew
//
//  Created by harry on 13-11-12.
//  Copyright (c) 2013年 harry. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MoreMobileViewController : UIViewController<UIAlertViewDelegate>

@property (weak, nonatomic) IBOutlet UITextField *txtMobile;
- (IBAction)clickUpdate:(id)sender;
- (IBAction)clickBack:(id)sender;
@end
