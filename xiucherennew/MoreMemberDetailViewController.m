//
//  MoreMemberDetailViewController.m
//  xiucherennew
//
//  Created by harry on 13-11-16.
//  Copyright (c) 2013年 harry. All rights reserved.
//

#import "MoreMemberDetailViewController.h"
#import "Member.h"

@interface MoreMemberDetailViewController ()

@end

@implementation MoreMemberDetailViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    /*
    _mb = [[Member alloc] init];
    _mb.nickname = @"呼啸天";
    _mb.age = @"29";
    _mb.gender = @"男";
    _mb.mail = @"xieyongjun001@gmail.com";
    _mb.stationcode = @"11111";
    _mb.stationname = @"常州";
    _mb.mobile = @"15687878749";
    */
    
    _lblAge.text = _mb.age;
    _lblGender.text = _mb.gender;
    _lblMail.text = _mb.mail;
    _lblMobile.text = _mb.mobile;
    _lblName.text = _mb.nickname;
    _lblStationCode.text = _mb.stationcode;
    _lblStationName.text = _mb.stationname;
    
    [_imgvHeader setImageWithURL:[NSURL URLWithString:_mb.headerimg] placeholderImage:[UIImage imageNamed:@"header"]];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)clickBack:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}
@end
