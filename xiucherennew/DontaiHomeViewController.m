//
//  DontaiHomeViewController.m
//  xiucherennew
//
//  Created by harry on 13-11-10.
//  Copyright (c) 2013年 harry. All rights reserved.
//

#import "DontaiHomeViewController.h"
#import "MoreMemberListViewController.h"
@interface DontaiHomeViewController ()

@end

@implementation DontaiHomeViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)clickMenu2:(id)sender {
}

- (IBAction)clickMenu1:(id)sender {
}
- (IBAction)clickMenu3:(id)sender {
}

- (IBAction)clickSixin:(id)sender {
    
    MoreMemberListViewController *mlvc = [[MoreMemberListViewController alloc] initWithNibName:@"MoreMemberListViewController" bundle:nil];
    mlvc.gosixin = 2;
    [self.navigationController pushViewController:mlvc animated:YES];
    
}
@end
