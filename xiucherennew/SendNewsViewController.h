//
//  SendNewsViewController.h
//  xiucherennew
//
//  Created by harry on 13-11-9.
//  Copyright (c) 2013年 harry. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SendNewsViewController : UIViewController<UIAlertViewDelegate,UIImagePickerControllerDelegate,UINavigationControllerDelegate>

@property (weak, nonatomic) IBOutlet UIView *toolview;
- (IBAction)clickCancel:(id)sender;
- (IBAction)clickSend:(id)sender;
- (IBAction)clickPhoto:(id)sender;
- (IBAction)clickAlbum:(id)sender;
- (IBAction)clickAt:(id)sender;
@property (weak, nonatomic) IBOutlet UIImageView *previewImg;
@property (weak, nonatomic) IBOutlet GCPlaceholderTextView *txtContent;
@property (strong, nonatomic) IBOutlet UITextField *newsTypeTxtFild;

@property (strong, nonatomic) UIPickerView *pickerView;
@end
