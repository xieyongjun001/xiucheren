//
//  KnowLedgeDetailViewController.h
//  xiucherennew
//
//  Created by harry on 13-11-11.
//  Copyright (c) 2013年 harry. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface KnowLedgeDetailViewController : UIViewController
@property(nonatomic,strong)NSString *knowledgeGuid;
@property(nonatomic,strong)NSString *knowlegeTitle;
@property (weak, nonatomic) IBOutlet UILabel *lblTitle;
@property (weak, nonatomic) IBOutlet UITextView *txtContent;
- (IBAction)clickBack:(id)sender;
@end
