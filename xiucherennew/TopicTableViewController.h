//
//  TopicTableViewController.h
//  xiucherennew
//
//  Created by apple on 14-5-14.
//  Copyright (c) 2014年 harry. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TopicTableViewController : UIViewController

@property (strong, nonatomic) IBOutlet UILabel *topicTitle;
@property (strong, nonatomic) IBOutlet UITableView *topicTableView;
@property (strong, nonatomic) NSMutableArray *topicArray;


-(id)initWithTypeId:(int)typeId;

@end
