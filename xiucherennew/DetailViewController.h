//
//  DetailViewController.h
//  xiucherennew
//
//  Created by apple on 14-5-19.
//  Copyright (c) 2014年 harry. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Post.h"

@interface DetailViewController : UIViewController

@property (weak, nonatomic) IBOutlet UIScrollView *sclMain;

- (IBAction)clickBack:(id)sender;
- (IBAction)clickResend:(id)sender;
- (IBAction)clickFeed:(id)sender;

@property (strong, nonatomic) Post *post;

@end
