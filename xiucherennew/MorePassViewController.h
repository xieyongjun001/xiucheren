//
//  MorePassViewController.h
//  xiucherennew
//
//  Created by harry on 13-11-12.
//  Copyright (c) 2013年 harry. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MorePassViewController : UIViewController<UIAlertViewDelegate>
- (IBAction)clickBack:(id)sender;
- (IBAction)clickUpdate:(id)sender;

@property (weak, nonatomic) IBOutlet UITextField *txtOldPass;
@property (weak, nonatomic) IBOutlet UITextField *txtNewPass;
@property (weak, nonatomic) IBOutlet UITextField *txtReNewPass;
@end
