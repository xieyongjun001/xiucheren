//
//  MoreFeedViewController.m
//  xiucherennew
//
//  Created by harry on 13-11-12.
//  Copyright (c) 2013年 harry. All rights reserved.
//

#import "MoreFeedViewController.h"

@interface MoreFeedViewController ()

@end

@implementation MoreFeedViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    _txtContent.placeholder = @"您的反馈意见";
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)clickCancel:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)clickSend:(id)sender {
    if ([_txtContent.text length]==0) {
        [Utils simpleAlert:@"请填写反馈意见"];
        return ;
    }
    
    NSDictionary *dict = @{@"scmeParentGuid": @"",@"scmeTitle": @"",@"scmeContent": _txtContent.text,@"scmePerson": @[[AppDelegate getAppDelegate].userCode]};
    NSString *output = [dict JSONRepresentation];
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    NSString *string = [NSString stringWithFormat:@"%@%@%@/%@",WEBSERVICE_URL,WEBSERVICE_savePrivateMsg,[output urlencode],[AppDelegate getAppDelegate].userCode];
    
    [manager POST:string parameters:nil success:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        NSLog(@"JSON: %@", responseObject);
        
        NSString *result = [responseObject objectForKey:@"resultDataType"];
        
        NSLog(@"jsonArray: %@", result);
        
        if([result isEqualToString:@"1"]){
            [Utils simpleAlert:@"密码更新成功" withDelegate:self];
            
        }
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"Error: %@", error);
    }];
}
@end
