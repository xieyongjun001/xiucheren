//
//  ForgetPass1ViewController.m
//  xiucherennew
//
//  Created by harry on 13-12-8.
//  Copyright (c) 2013年 harry. All rights reserved.
//

#import "ForgetPass1ViewController.h"
#import "ForgetPass2ViewController.h"
@interface ForgetPass1ViewController ()

@end

@implementation ForgetPass1ViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)clickBack:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)clickSubmit:(id)sender {

    
    if ([_txtAccount.text length]==0 || [_txtRecommend.text length]==0 ||
        [_txtName.text length]==0 || [_txtMobile.text length]==0 || [_txtEmail.text length]==0) {
            [Utils simpleAlert:@"请填写所有内容"];
        return ;
    }
    
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    //{userCode}/{rmdUserCode}/{userName}/{mobile}/{email}")
    NSString *string = [NSString stringWithFormat:@"%@%@%@/%@/%@/%@/%@",WEBSERVICE_URL,WEBSERVICE_ForgetPass,_txtAccount.text,_txtRecommend.text,_txtName.text,_txtMobile.text,_txtEmail.text];
    
    [manager GET:string parameters:nil success:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        NSLog(@"JSON: %@", responseObject);
        
        NSString *result = [responseObject objectForKey:@"resultDataType"];
        
        NSLog(@"jsonArray: %@", result);
        
        if([result isEqualToString:@"1"]){
            NSArray *resultList = [responseObject objectForKey:@"resultList"];
            NSDictionary *resultdic = [resultList objectAtIndex:0];
            NSString *msg = [NSString stringWithFormat:@"申请成功，您的新密码是：%@",[resultdic objectForKey:@"auserPassword"]];
           [Utils simpleAlert:msg withDelegate:self];
        }
        
        if([result isEqualToString:@"-1"]){
            [Utils simpleAlert:@"申请失败，请确认填写的信息是否正确！"];
        }
        
        [MBProgressHUD hideHUDForView:self.view animated:YES];
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"Error: %@", error);
        [MBProgressHUD hideHUDForView:self.view animated:YES];
    }];

}

- (void)touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event{
    [_txtAccount resignFirstResponder];
    [_txtRecommend resignFirstResponder];
    [_txtName resignFirstResponder];
    [_txtMobile resignFirstResponder];
    [_txtEmail resignFirstResponder];
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    ForgetPass2ViewController *rsvc = [[ForgetPass2ViewController alloc] initWithNibName:@"ForgetPass2ViewController" bundle:nil];
    [self.navigationController pushViewController:rsvc animated:YES];
}
@end
