//
//  CheckinHomeViewController.h
//  xiucherennew
//
//  Created by harry on 13-11-21.
//  Copyright (c) 2013年 harry. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CheckinHomeViewController : UIViewController
@property (weak, nonatomic) IBOutlet UILabel *txtCheckinSuccess;
@property (weak, nonatomic) IBOutlet UIImageView *imgvTick;
@property (weak, nonatomic) IBOutlet UIButton *btCheckin;
@property (weak, nonatomic) IBOutlet UILabel *lblTime;

@property (weak, nonatomic) IBOutlet UILabel *txtDate;
- (IBAction)clickBack:(id)sender;
- (IBAction)clickCheckin:(id)sender;
- (IBAction)clickPoint:(id)sender;
@end
