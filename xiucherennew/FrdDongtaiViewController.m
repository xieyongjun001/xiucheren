//
//  FrdDongtaiViewController.m
//  xiucherennew
//
//  Created by harry on 13-11-10.
//  Copyright (c) 2013年 harry. All rights reserved.
//

#import "FrdDongtaiViewController.h"
#import "Post.h"
#import "CommentListViewController.h"
#import "CommentSendViewController.h"
#import "ResentViewController.h"

@interface FrdDongtaiViewController ()
@property (strong, nonatomic) UIScrollView *sclview;
@property (strong, nonatomic) NSMutableArray *postlist;
@end

@implementation FrdDongtaiViewController
@synthesize sclview;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self.navigationController setNavigationBarHidden:YES];
	// Do any additional setup after loading the view.
    // Do any additional setup after loading the view.
    sclview = [[UIScrollView alloc] initWithFrame:CGRectMake(0, 75, 320, 450)];
    sclview.backgroundColor = [UIColor clearColor];
    sclview.bounces = YES;
    [self.view addSubview:sclview];
}

- (void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    
    
    
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    NSString *string = [NSString stringWithFormat:@"%@%@%@/friendPage",WEBSERVICE_URL,WEBSERVICE_GetPost,[AppDelegate getAppDelegate].userCode];
    
    [manager GET:string parameters:nil success:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        NSLog(@"JSON: %@", responseObject);
        
        NSString *result = [responseObject objectForKey:@"resultDataType"];
        
        NSLog(@"jsonArray: %@", result);
        
        if([result isEqualToString:@"1"]){
            _postlist = [responseObject objectForKey:@"resultList"];
        }
        [self reloadViews];
        
        [MBProgressHUD hideHUDForView:self.view animated:YES];
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"Error: %@", error);
        [MBProgressHUD hideHUDForView:self.view animated:YES];
    }];
}

- (void)reloadViews{
    UIView *v;
	while ((v = [[sclview subviews] lastObject])) {
		[v removeFromSuperview];
	}
    
    float y=0;
    for (int i=0; i<[_postlist count]; i++) {
        NSDictionary *dic = [_postlist objectAtIndex:i];
        
        Post *post = [[Post alloc] init];
        post.content = [dic objectForKey:@"wordContent"];
        post.nickname = [dic objectForKey:@"postCreaterName"];;
        post.date = [dic objectForKey:@"createTime"];
        post.headerpath = [dic objectForKey:@"userImg"];
        post.postid = [dic objectForKey:@"postGuid"];
        post.zhfnum = [dic objectForKey:@"postZhuanfaNum"];
        post.huifunum = [dic objectForKey:@"postHuifuNum"];
        post.zannum = [dic objectForKey:@"postZanNum"];
        post.imageContent = [dic objectForKey:@"imageContent"];
        post.zanstatus = [[dic objectForKey:@"postInfoUserZanStatus"] intValue];
        
        NSDictionary *attributesDictionary = [NSDictionary dictionaryWithObjectsAndKeys:
                                              [UIFont systemFontOfSize:14], NSFontAttributeName,
                                              nil];
        
        
        CGFloat height = 0;
        if (![post.content isEqual: [NSNull null]] && post.content!=nil && [post.content length]>0) {
            NSMutableAttributedString *string = [[NSMutableAttributedString alloc] initWithString:post.content attributes:attributesDictionary];
            CGRect paragraphRect =
            [string boundingRectWithSize:CGSizeMake(300.f, CGFLOAT_MAX)
                                 options:(NSStringDrawingUsesLineFragmentOrigin|NSStringDrawingUsesFontLeading)
                                 context:nil];
            height = paragraphRect.size.height + 44;
        }
        
        float h = MSGBOX_BOTTOMHEIGH+MSGBOX_TOPHEIGH+height;
        if (![post.imageContent isEqualToString:@""]) {
            h+= MSGBOX_IMGHEIGH;
        }
        
        
        MessageView *mv = [[MessageView alloc] initWithFrame:CGRectMake(7, y, 307, h) withData:post];
        
        if ([post.imageContent isEqualToString:@""]) {
            y+=MSGBOX_BOTTOMHEIGH+MSGBOX_TOPHEIGH+height+MSGBOX_PADDING;
        }else{
            y+=MSGBOX_BOTTOMHEIGH+MSGBOX_TOPHEIGH+height+MSGBOX_IMGHEIGH+MSGBOX_PADDING;
        }
        [sclview addSubview:mv];
    }
    sclview.contentSize = CGSizeMake(320, y+40);
    
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)clickBack:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark messageview delgate

- (void)didClickRight:(NSString *)index{
    CommentSendViewController *clvc = [[CommentSendViewController alloc] initWithNibName:@"CommentSendViewController" bundle:nil];
    clvc.postGuid = index;
    [self.navigationController pushViewController:clvc animated:YES];
    
}

- (void)didClickLeft:(Post *)postData{
    ResentViewController *rsvc = [[ResentViewController alloc] initWithNibName:@"ResentViewController" bundle:nil];
    rsvc.data = postData;
    [self.navigationController pushViewController:rsvc animated:YES];
}

- (void)didClickCenter:(Post *)postData{
    CommentListViewController *rsvc = [[CommentListViewController alloc] initWithNibName:@"CommentListViewController" bundle:nil];
    rsvc.post = postData;
    [self.navigationController pushViewController:rsvc animated:YES];
}
@end
