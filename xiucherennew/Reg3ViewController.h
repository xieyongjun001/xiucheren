//
//  Reg3ViewController.h
//  xiucherennew
//
//  Created by harry on 13-11-9.
//  Copyright (c) 2013年 harry. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface Reg3ViewController : UIViewController<UIPickerViewDelegate, UIPickerViewDataSource>
@property (strong, nonatomic)NSString *querystring;
@property (weak, nonatomic) IBOutlet UITextField *txtCompanyType;
@property (weak, nonatomic) IBOutlet UITextField *txtArea;
@property (weak, nonatomic) IBOutlet UITextField *txtProvince;
@property (weak, nonatomic) IBOutlet UITextField *txtCity;

@property (weak, nonatomic) IBOutlet UITextField *txtBrand;
@property (weak, nonatomic) IBOutlet UITextField *txtCompany;
@property (weak, nonatomic) IBOutlet UILabel *lblCompanyCode;
@property (weak, nonatomic) IBOutlet UILabel *lblCompanyAddress;
@property (weak, nonatomic) IBOutlet UIScrollView *scl;
- (IBAction)clickNext:(id)sender;
- (IBAction)clickBack:(id)sender;

@end
