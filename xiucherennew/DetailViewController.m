//
//  DetailViewController.m
//  xiucherennew
//
//  Created by apple on 14-5-19.
//  Copyright (c) 2014年 harry. All rights reserved.
//

#import "DetailViewController.h"
#import "CommentSendViewController.h"
#import "ResentViewController.h"
#import "DetailView.h"
#import "CommentView.h"
#import <ShareSDK/ShareSDK.h>

@interface DetailViewController (){
    UIView *cmtview;
    int islike;
    float lasty;
    float msgboxh;
}


@end

@implementation DetailViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    
    
    NSDictionary *attributesDictionary = [NSDictionary dictionaryWithObjectsAndKeys:
                                          [UIFont systemFontOfSize:14], NSFontAttributeName,
                                          nil];
    
    
    CGFloat height = 0;
    if (![_post.content isEqual: [NSNull null]] && _post.content!=nil && [_post.content length]>0) {
        NSMutableAttributedString *string = [[NSMutableAttributedString alloc] initWithString:_post.content attributes:attributesDictionary];
        CGRect paragraphRect =
        [string boundingRectWithSize:CGSizeMake(300.f, CGFLOAT_MAX)
                             options:(NSStringDrawingUsesLineFragmentOrigin|NSStringDrawingUsesFontLeading)
                             context:nil];
        height = paragraphRect.size.height + 44;
    }
    
    float h = MSGBOX_BOTTOMHEIGH+MSGBOX_TOPHEIGH+height;
    if (![_post.imageContent isEqualToString:@""]) {
        h+= MSGBOX_IMGHEIGH;
    }
    msgboxh = h;
    
    DetailView *mv = [[DetailView alloc] initWithFrame:CGRectMake(7, 3, 307, h) withData:_post];
    
    
    [_sclMain addSubview:mv];
    
    cmtview = [[UIView alloc] initWithFrame:CGRectMake(7, h+10, 307, 400)];
    cmtview.backgroundColor = [Utils colorWithHexString:@"f6f6f6"];
    [_sclMain addSubview:cmtview];
    
    UIImage *lineimg = [UIImage imageNamed:@"weibo_content_line.png"];
    UIImageView *imgv = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, lineimg.size.width, lineimg.size.height)];
    imgv.image = lineimg;
    [cmtview addSubview:imgv];
    
    lasty = imgv.frame.size.height + imgv.frame.origin.y + 4;
    
//    if (_post.zanstatus==1) {
//        UIImage *likeimg=[UIImage imageNamed:@"simpleLikeRed.png"];
//        [_imgLike setImage:likeimg];
//    }
    
    float y=5;
    UILabel *lblzf = [[UILabel alloc] initWithFrame:CGRectMake(5, y,60,20)];
    lblzf.font = [UIFont systemFontOfSize:12];
    lblzf.textColor = [UIColor darkGrayColor];
    lblzf.backgroundColor = [UIColor clearColor];
    lblzf.opaque = NO;
    lblzf.textAlignment = NSTextAlignmentLeft;
    lblzf.text = [NSString stringWithFormat:@"%d 转发",[_post.zhfnum intValue]];
    [cmtview addSubview:lblzf];
    
    
    UILabel *lblpl = [[UILabel alloc] initWithFrame:CGRectMake(70, y,60,20)];
    lblpl.font = [UIFont systemFontOfSize:12];
    lblpl.textColor = [UIColor blackColor];
    lblpl.backgroundColor = [UIColor clearColor];
    lblpl.opaque = NO;
    lblpl.textAlignment = NSTextAlignmentCenter;
    lblpl.text = [NSString stringWithFormat:@"%d 评论",[_post.huifunum intValue]];
    [cmtview addSubview:lblpl];
    
    
//    UILabel *lbllk = [[UILabel alloc] initWithFrame:CGRectMake(255, y,60,20)];
//    lbllk.font = [UIFont systemFontOfSize:12];
//    lbllk.textColor = [UIColor darkGrayColor];
//    lbllk.backgroundColor = [UIColor clearColor];
//    lbllk.opaque = NO;
//    lbllk.textAlignment = NSTextAlignmentCenter;
//    lbllk.text = [NSString stringWithFormat:@"%d 赞",[_post.zannum intValue]];
//    [cmtview addSubview:lbllk];
    
    
}

- (void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    NSLog(@"post:%@",_post);
    
    //_lblfeed.text = [NSString stringWithFormat:@"%d 评论",[_post.huifunum intValue]];
    //_lblresend.text = [NSString stringWithFormat:@"%d 转发",[_post.zhfnum intValue]];
    
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    NSString *string = [NSString stringWithFormat:@"%@%@%@",WEBSERVICE_URL,WEBSERVICE_CmtList,_post.postid];
    NSLog(@"string: %@", string);
    
    [manager GET:string parameters:nil success:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        NSLog(@"JSON: %@", responseObject);
        
        NSArray *resultarr = [responseObject objectForKey:@"resultList"];
        
        for (int i=0; i<[resultarr count]; i++) {
            NSDictionary *data = [resultarr objectAtIndex:i];
            int strlen = [Utils getStrLen:data[@"tieContent"]];
            //NSLog(@"%@ %f",itemInfo.shortAddress,ceilf(strlen/20.0));
            float h = 24*ceil(strlen/20.0);
            if (h==0) {
                h=24;
            }
            CommentView *cmv = [[CommentView alloc] initWithFrame:CGRectMake(0, lasty, cmtview.frame.size.width, 48+h) withData:data];
            
            lasty+=(48+h);
            [cmtview addSubview:cmv];
        }
        CGRect a = cmtview.frame;
        a.size.height = lasty;
        cmtview.frame = a;
        
        
        
        _sclMain.contentSize = CGSizeMake(_sclMain.frame.size.width, lasty+msgboxh+100);
        //NSLog(@"jsonArray: %@", result);
        
        
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"Error: %@", error);
    }];
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


//- (void) likeClick{
//    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
//    NSString *string = [NSString stringWithFormat:@"%@%@%@/%@",WEBSERVICE_URL,WEBSERVICE_PostZan,[AppDelegate getAppDelegate].userCode,_post.postid];
//    
//    [manager POST:string parameters:nil success:^(AFHTTPRequestOperation *operation, id responseObject) {
//        
//        NSLog(@"JSON: %@", responseObject);
//        
//        UIImage *likeimg=[UIImage imageNamed:@"simpleLikeRed.png"];
//        [_imgLike setImage:likeimg];
//        
//        islike = 1;
//        
//    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
//        NSLog(@"Error: %@", error);
//    }];
//    
//}
//
//- (void) unlikeClick{
//    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
//    NSString *string = [NSString stringWithFormat:@"%@%@%@/%@",WEBSERVICE_URL,WEBSERVICE_CancelZan,[AppDelegate getAppDelegate].userCode,_post.postid];
//    
//    [manager POST:string parameters:nil success:^(AFHTTPRequestOperation *operation, id responseObject) {
//        
//        NSLog(@"JSON: %@", responseObject);
//        
//        UIImage *likeimg=[UIImage imageNamed:@"simpleLike.png"];
//        [_imgLike setImage:likeimg];
//        
//        islike = 0;
//        
//    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
//        NSLog(@"Error: %@", error);
//    }];
//}

- (IBAction)clickBack:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)clickResend:(id)sender {
    /*   [Utils notOpen];
     
     ResentViewController *rsvc = [[ResentViewController alloc] initWithNibName:@"ResentViewController" bundle:nil];
     rsvc.data = _post;
     [self.navigationController pushViewController:rsvc animated:YES];
     */
    
    NSString *imagePath = @"";
    if (![_post.imageContent isEqualToString:@""]) {
        NSArray *arr = [_post.imageContent componentsSeparatedByString:@";"];
        imagePath = [arr objectAtIndex:0];
    }
    
    
    //构造分享内容
    id<ISSContent> publishContent = [ShareSDK content:_post.content
                                       defaultContent:_post.content
                                                image:[ShareSDK imageWithUrl:imagePath]
                                                title:@"修车人"
                                                  url:[NSString stringWithFormat:@"%@%@",SHARE_WEIBO,_post.postid]
                                          description:_post.content
                                            mediaType:SSPublishContentMediaTypeNews];
    
    [ShareSDK showShareActionSheet:nil
                         shareList:nil
                           content:publishContent
                     statusBarTips:YES
                       authOptions:nil
                      shareOptions: nil
                            result:^(ShareType type, SSResponseState state, id<ISSPlatformShareInfo> statusInfo, id<ICMErrorInfo> error, BOOL end) {
                                if (state == SSResponseStateSuccess)
                                {
                                    NSLog(@"分享成功");
                                }
                                else if (state == SSResponseStateFail)
                                {
                                    NSLog(@"分享失败,错误码:%d,错误描述:%@", [error errorCode], [error errorDescription]);
                                }
                            }];
    
}

- (IBAction)clickFeed:(id)sender {
    CommentSendViewController *clvc = [[CommentSendViewController alloc] initWithNibName:@"CommentSendViewController" bundle:nil];
    clvc.postGuid = _post.postid;
    [self.navigationController pushViewController:clvc animated:YES];
}


@end
