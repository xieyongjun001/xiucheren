//
//  JobManViewController.h
//  xiucherennew
//
//  Created by harry on 13-11-16.
//  Copyright (c) 2013年 harry. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface JobManViewController : UIViewController<UITableViewDataSource,UITableViewDelegate>
@property (strong, nonatomic) NSArray *jobs;
@property (weak, nonatomic) IBOutlet UITableView *tbview;
- (IBAction)clickBack:(id)sender;
- (IBAction)clickSend:(id)sender;
@end
