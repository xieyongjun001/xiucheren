//
//  Reg1ViewController.m
//  xiucherennew
//
//  Created by harry on 13-11-9.
//  Copyright (c) 2013年 harry. All rights reserved.
//

#import "Reg1ViewController.h"
#import "Reg2ViewController.h"
@interface Reg1ViewController ()

@end

@implementation Reg1ViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)clickBack:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)clickNext:(id)sender {
    if ([_txtRecman.text length]==0) {
        [Utils simpleAlert:@"请填写推荐人帐号"];
        return ;
    }
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    NSString *string = [NSString stringWithFormat:@"%@%@%@",WEBSERVICE_URL,WEBSERVICE_Reg1,_txtRecman.text];
    [manager GET:string parameters:nil success:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        NSLog(@"JSON: %@", responseObject);
        
        NSString *result = [responseObject objectForKey:@"resultDataType"];
        
        NSLog(@"jsonArray: %@", result);
        
        if([result isEqualToString:@"1"]){
            NSArray *resultlist = [responseObject objectForKey:@"resultList"];
            NSLog(@"resultlist:%@",resultlist);
            
            
            Reg2ViewController *rvc = [[Reg2ViewController alloc] initWithNibName:@"Reg2ViewController" bundle:nil];
            rvc.isedit = 0;
            [self.navigationController pushViewController:rvc animated:YES];
            
        }else{
            [Utils simpleAlert:@"推荐人不存在"];
        }
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"Error: %@", error);
    }];
    
    
}


@end
