//
//  FirstViewController.m
//  xiucherennew
//
//  Created by harry on 13-11-9.
//  Copyright (c) 2013年 harry. All rights reserved.
//

#import "FirstViewController.h"

@interface FirstViewController ()

@end

@implementation FirstViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view, typically from a nib.
   
    
}

- (void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    
    UIImage *unselC = [UIImage imageNamed:@"tab1iconoff.png"];
    UIImage *selC = [UIImage imageNamed:@"tab1iconon.png"];
    
    [self.tabBarItem setSelectedImage:selC];
    [self.tabBarItem setImage:unselC];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
