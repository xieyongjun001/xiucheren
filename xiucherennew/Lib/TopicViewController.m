//
//  TopicViewController.m
//  xiucherennew
//
//  Created by apple on 14-5-13.
//  Copyright (c) 2014年 harry. All rights reserved.
//

#import "TopicViewController.h"
#import "TopicTableViewController.h"
@interface TopicViewController ()

@end

@implementation TopicViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    //self.navigationController.navigationBar.tintColor=[UIColor lightGrayColor];
//    UIButton *backButtion = [[UIButton alloc] init];
//    backButtion.frame = CGRectMake(0, 0, 44, 44);
//    [backButtion setImage:[UIImage imageNamed:@"reg1_back"] forState:UIControlStateNormal];
//    UIBarButtonItem *backButtionItem = [[UIBarButtonItem alloc] initWithCustomView:backButtion];
//    
//    self.navigationItem.backBarButtonItem = backButtionItem;
    
//    self.navigationItem.backBarButtonItem =
//    [[UIBarButtonItem alloc] initWithTitle:nil
//                                     style:UIBarButtonItemStylePlain
//                                    target:nil
//                                    action:nil];
    
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

//* @param topicType 是否公开(1.公开 0.不公开)
- (IBAction)publicTopicAciton:(id)sender {
    
    TopicTableViewController *topicTVC = [[TopicTableViewController alloc] initWithTypeId:1];
    [self.navigationController pushViewController:topicTVC animated:YES];
}
- (IBAction)privateTopicAction:(id)sender {
    TopicTableViewController *topicTVC = [[TopicTableViewController alloc] initWithTypeId:0];
    [self.navigationController pushViewController:topicTVC animated:YES];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
