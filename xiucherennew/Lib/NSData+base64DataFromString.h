//
//  NSData+base64DataFromString.h
//  xiucherennew
//
//  Created by harry on 14-4-2.
//  Copyright (c) 2014年 harry. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSData (base64DataFromString)
+ (NSData *)base64DataFromString: (NSString *)string;
@end
