//
//  Utils.h
//  xiucherennew
//
//  Created by harry on 13-11-16.
//  Copyright (c) 2013年 harry. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Utils : NSObject
+(void)	notOpen;
+(void)	simpleAlert:(NSString *)msg;
+(void)	simpleAlert:(NSString *)msg withDelegate:(id)delegate;

+(UIColor *)	colorWithHexString: (NSString *) stringToConvert;
+ (int)getStrLen:(NSString  *)str;

+(NSDate * )NSStringDateToNSDate: (NSString * )string withFormatter:(NSString *)format;
@end
