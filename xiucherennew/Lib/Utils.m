//
//  Utils.m
//  xiucherennew
//
//  Created by harry on 13-11-16.
//  Copyright (c) 2013年 harry. All rights reserved.
//

#import "Utils.h"
#define kDEFAULT_DATE_TIME_FORMAT (@"yyyy-MM-dd")
@implementation Utils
+(void)	notOpen{
    UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"提示" message:@"功能尚未开通" delegate:nil cancelButtonTitle:@"知道了" otherButtonTitles:nil];
    [alertView show];
}

+(void)	simpleAlert:(NSString *)msg{
    UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"提示" message:msg delegate:nil cancelButtonTitle:@"确定" otherButtonTitles:nil];
    [alertView show];
}

+(void)	simpleAlert:(NSString *)msg withDelegate:(id)delegate{
    UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"提示" message:msg delegate:delegate cancelButtonTitle:@"确定" otherButtonTitles:nil];
    [alertView show];
}

+ (UIColor *) colorWithHexString: (NSString *) stringToConvert
{
	NSString *cString = [[stringToConvert stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]] uppercaseString];
	
	// String should be 6 or 8 characters
	if ([cString length] < 6) return [UIColor blackColor];
	
	// strip 0X if it appears
	if ([cString hasPrefix:@"0X"]) cString = [cString substringFromIndex:2];
	
	if ([cString length] != 6) return [UIColor blackColor];
	
	// Separate into r, g, b substrings
	NSRange range;
	range.location = 0;
	range.length = 2;
	NSString *rString = [cString substringWithRange:range];
	
	range.location = 2;
	NSString *gString = [cString substringWithRange:range];
	
	range.location = 4;
	NSString *bString = [cString substringWithRange:range];
	
	// Scan values
	unsigned int r, g, b;
	[[NSScanner scannerWithString:rString] scanHexInt:&r];
	[[NSScanner scannerWithString:gString] scanHexInt:&g];
	[[NSScanner scannerWithString:bString] scanHexInt:&b];
	
	return [UIColor colorWithRed:((float) r / 255.0f)
						   green:((float) g / 255.0f)
							blue:((float) b / 255.0f)
						   alpha:1.0f];
}

//按照指定格式返回时间 (日期+具体时间",如"2月11日 20:19")
+(NSString *)speTimewithTimeStr:(NSString *)timeStr{
    NSDateFormatter *formatter = [[NSDateFormatter alloc]init];
    [formatter setDateFormat:@"MM月dd日 HH:mm:ss"];
    NSTimeInterval timeInterval = [timeStr intValue];
    NSDate *confromTimesp = [NSDate dateWithTimeIntervalSince1970:timeInterval];
    NSString *str =  [formatter stringFromDate:confromTimesp];
    return str;
}

//返回时间所在日期当天的准确时间. (20:19)
+(NSString *)smallTimewithTimeStr:(NSString *)timeStr{
    NSDateFormatter *formatter = [[NSDateFormatter alloc]init];
    [formatter setDateFormat:@" HH:mm:ss"];
    NSTimeInterval timeInterval = [timeStr intValue];
    NSDate *confromTimesp = [NSDate dateWithTimeIntervalSince1970:timeInterval];
    NSString *str =  [formatter stringFromDate:confromTimesp];
    return str;
}


//判断两个日期是否是同一天
+(BOOL)isSameDay:(NSDate*)date1 date2:(NSDate*)date2
{
    NSCalendar* calendar = [NSCalendar currentCalendar];
    unsigned unitFlags = NSYearCalendarUnit | NSMonthCalendarUnit |  NSDayCalendarUnit;
    NSDateComponents* comp1 = [calendar components:unitFlags fromDate:date1];
    NSDateComponents* comp2 = [calendar components:unitFlags fromDate:date2];
    return [comp1 day]   == [comp2 day] &&
    [comp1 month] == [comp2 month] &&
    [comp1 year]  == [comp2 year];
}

+ (int)getStrLen:(NSString  *)str{
    int len=0;
    for(int i=0; i< [str length];i++){
        int a = [str characterAtIndex:i];
        if( a > 0x4e00 && a < 0x9fff){
            len+=2;
        }else {
            len+=1;
        }
    }
    return len;
}

//时间转时间戳
+(NSString *)timeStrTotimeStamp:(id)timeStr{
    //获取当前时间时间串
    //与获得时间串进行对比
    //根据时间差显示不同内容
    //返回内容
//    int nowTime = [[DSUtils timeStamp] intValue];
//    int gapTime = nowTime - [timeStr intValue];
//    NSString *timeStamp = nil;
//    if (gapTime < 60) {
//        //一分钟内显示刚刚
//        timeStamp = [NSString stringWithFormat:@"刚刚"];
//    }else if(60<=gapTime && gapTime<60*60){
//        //一分钟以上且一个小时之内的，显示“多少分钟前”，例如“5分钟前”
//        timeStamp = [NSString stringWithFormat:@"%i分钟前",gapTime/60];
//    }else if (60*60<=gapTime && gapTime<60*60*24*3){
//        //1小时以上三天以内的显示“今天/昨天/前天+具体时间”
//        NSString *dayStr ;
//        int gapDay = gapTime/(60*60*24) ;
//        switch (gapDay) {
//            case 0:
//            {
//                //在24小时内,存在跨天的现象. 判断两个时间是否在同一天内.
//                NSDate *date1 = [NSDate date];
//                NSTimeInterval timeInterval = [timeStr intValue];
//                NSDate *date2 = [NSDate dateWithTimeIntervalSince1970:timeInterval];
//                BOOL idSameDay = [DSUtils isSameDay:date1 date2:date2];
//                if (idSameDay == YES) {
//                    dayStr = [NSString stringWithFormat:@"今天"];
//                }else{
//                    dayStr = [NSString stringWithFormat:@"昨天"];
//                }
//            }
//                break;
//            case 1:
//                dayStr = [NSString stringWithFormat:@"昨天"];
//                break;
//            case 2:
//                dayStr = [NSString stringWithFormat:@"前天"];
//                break;
//            default:
//                break;
//        }
//        //        timeStamp = [dayStr stringByAppendingString:[DSUtils smallTimewithTimeStr:timeStrs]];
//        timeStamp = [NSString stringWithFormat:@"%@%@",dayStr,[DSUtils smallTimewithTimeStr:timeStr]];
//    }else{
//        //前天以后的显示"日期+具体时间",如"2月11日 20:19"
//        timeStamp = [NSString stringWithString:[DSUtils speTimewithTimeStr:timeStr]];
//    }
//    return [timeStamp copy];
    return @"";
}

+ (NSDate * )NSStringDateToNSDate: (NSString * )string withFormatter:(NSString *)format{
    
    NSDateFormatter * formatter = [[NSDateFormatter alloc] init];
    if ([format length]>0) {
        [formatter setDateFormat:format];
    }else{
        [formatter setDateFormat: kDEFAULT_DATE_TIME_FORMAT];
    }
    NSDate *date = [formatter dateFromString :string ];
    return date;
    
    
}
@end
