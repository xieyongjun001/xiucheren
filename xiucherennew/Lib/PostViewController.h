//
//  PostViewController.h
//  xiucherennew
//
//  Created by apple on 14-5-13.
//  Copyright (c) 2014年 harry. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PostViewController : UIViewController
@property (strong, nonatomic) IBOutlet UITextField *topicType;

@property (strong, nonatomic) IBOutlet UITextField *titleTxtFld;
@property (strong, nonatomic) IBOutlet UITextView *contentTxtView;
@property (strong, nonatomic) IBOutlet UIImageView *selectedImageView;

@property (nonatomic,strong) NSString *savepath;

@property (strong, nonatomic) UIPickerView *pickerView;

- (id)initWithTypeId:(int)typeId;

@end
