//
//  PostViewController.m
//  xiucherennew
//
//  Created by apple on 14-5-13.
//  Copyright (c) 2014年 harry. All rights reserved.
//

#import "PostViewController.h"
#import "UIImage+Thumb.h"

@interface PostViewController ()<UIImagePickerControllerDelegate,UINavigationControllerDelegate,UIPickerViewDataSource,UIPickerViewDelegate>{

    int _seletedType;
    int _typeId;
    
    NSArray *_topicTypeArray;
}

@end

@implementation PostViewController

@synthesize topicType;
@synthesize selectedImageView;
@synthesize pickerView;
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (id)initWithTypeId:(int)typeId
{
    self = [super init];
    if (self) {
        _typeId = typeId;
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.navigationItem.title = @"新话题";
    
    _topicTypeArray = @[@"内部",@"外部"];
    
    //* @param topicType 是否公开(1.公开 0.不公开)
    if (_typeId==0) {
        _seletedType = 0;
      
    }else{
        _seletedType = 1;
        
    }
    
    pickerView = [[UIPickerView alloc] init];
    pickerView.backgroundColor = [UIColor lightGrayColor];
    pickerView.autoresizingMask = UIViewAutoresizingFlexibleHeight|UIViewAutoresizingFlexibleWidth;
    pickerView.dataSource = self;
    pickerView.delegate = self;
    pickerView.showsSelectionIndicator = YES;
    self.topicType.inputView = pickerView;
    
    UILabel *leftLabelForType = [[UILabel alloc] init];
    leftLabelForType.frame = CGRectMake(0, 0, 10, 30);
    
    self.topicType.leftView = leftLabelForType;
    self.topicType.leftViewMode = UITextFieldViewModeAlways;
    
    
    UIImageView *rightImageView = [[UIImageView alloc] init];
    rightImageView.frame = CGRectMake(0, 0, 30, 29);
    rightImageView.image = [UIImage imageNamed:@"reg3_downarr.png"];
    self.topicType.rightView = rightImageView;
    self.topicType.rightViewMode = UITextFieldViewModeAlways;
    
    
    UIToolbar *toolBarForType = [[UIToolbar alloc]initWithFrame:CGRectMake(0, 0, 320, 44)];
    UIBarButtonItem *flexSpace = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:self action:nil];
    UIBarButtonItem *rightButtonItemForType = [[UIBarButtonItem alloc] initWithTitle:@"确定" style:UIBarButtonItemStylePlain target:self action:@selector(doItForType)];
    toolBarForType.items = [NSArray arrayWithObjects:flexSpace,rightButtonItemForType, nil];
    
    self.topicType.inputAccessoryView = toolBarForType;

    
    UILabel *leftLabel = [[UILabel alloc] init];
    leftLabel.frame = CGRectMake(0, 0, 10, 30);
    
    self.titleTxtFld.leftView = leftLabel;
    self.titleTxtFld.leftViewMode = UITextFieldViewModeAlways;
    
    UIToolbar *toolBar = [[UIToolbar alloc]initWithFrame:CGRectMake(0, 0, 320, 44)];
    [toolBar setBackgroundColor:[UIColor colorWithPatternImage:[UIImage imageNamed:@"sendnews_toolbg"]]];
    
    UIButton *takePhotoButton = [[UIButton alloc] init];
    takePhotoButton.frame = CGRectMake(20, 11, 28, 22);
    [takePhotoButton addTarget:self action:@selector(takePhoto) forControlEvents:UIControlEventTouchUpInside];
    [takePhotoButton setImage:[UIImage imageNamed:@"sendnews_photo"] forState:UIControlStateNormal];
    UIBarButtonItem *takePhotoButtonItem = [[UIBarButtonItem alloc] initWithCustomView:takePhotoButton];
    
    UIButton *takePictureButton = [[UIButton alloc] init];
    takePictureButton.frame = CGRectMake(73, 11, 28, 22);
    [takePictureButton addTarget:self action:@selector(takeAlbum) forControlEvents:UIControlEventTouchUpInside];
    [takePictureButton setImage:[UIImage imageNamed:@"sendnews_album"] forState:UIControlStateNormal];
    UIBarButtonItem *takePictureButtonItem = [[UIBarButtonItem alloc] initWithCustomView:takePictureButton];
    
    UIBarButtonItem *rightButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"确定" style:UIBarButtonItemStylePlain target:self action:@selector(doIt)];
    
    toolBar.items = [NSArray arrayWithObjects:flexSpace,takePhotoButtonItem,flexSpace,takePictureButtonItem,flexSpace,flexSpace,flexSpace,flexSpace,flexSpace,flexSpace,flexSpace,flexSpace,flexSpace,flexSpace,flexSpace,flexSpace,rightButtonItem, nil];
    
    self.contentTxtView.inputAccessoryView = toolBar;
}

-(void)doItForType
{
    if ([self.topicType.text length]==0) {
        self.topicType.text = [_topicTypeArray objectAtIndex:0];

    }
    [self.topicType resignFirstResponder];
    
}

-(void)doIt
{
    [self.contentTxtView resignFirstResponder];
    
}

- (IBAction)cancelButtonAction:(id)sender {
    
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)sendButtonAction:(id)sender {
    
    if ([self.topicType.text length]==0||[self.titleTxtFld.text length]==0||[self.contentTxtView.text length]==0) {
        [Utils simpleAlert:@"请填写话题的标题或内容！"];
        return ;
    }
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    if (_savepath) {
        AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
        NSURL *filePath = [NSURL fileURLWithPath:_savepath];
        [manager POST:WEBSERVICE_UploadPhoto parameters:nil constructingBodyWithBlock:^(id<AFMultipartFormData> formData) {
            [formData appendPartWithFileURL:filePath name:@"userdefine" error:nil];
        } success:^(AFHTTPRequestOperation *operation, id responseObject) {
            NSLog(@"Success: %@", responseObject);
            
            NSArray *resultarr = [responseObject objectForKey:@"resultList"];
            [self submit:resultarr];
            
        } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
            NSLog(@"Error: %@", error);
        }];
    }else{
        [self submit:nil];
    }
    
    
}

- (void)submit:(NSArray *)dic{
    
    //@Path("/publishTopic/{userCode}/{wordContent}")
    NSString *string = [NSString stringWithFormat:@"%@%@/%@/%@/%@/%d",WEBSERVICE_URL,WEBSERVICE_PostTopic,[AppDelegate getAppDelegate].userCode,[self.contentTxtView.text urlencode],[self.titleTxtFld.text urlencode],_seletedType];
    if (dic==nil) {
        string = [NSString stringWithFormat:@"%@%@/%@/%@/%@/%d",WEBSERVICE_URL,WEBSERVICE_PostTopic,[AppDelegate getAppDelegate].userCode,[self.contentTxtView.text urlencode],[self.titleTxtFld.text urlencode],_seletedType];
    }
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    manager.requestSerializer = [AFJSONRequestSerializer serializer];
    
    
    [manager POST:string parameters:dic
          success:^(AFHTTPRequestOperation *operation, id responseObject)
     {
         NSLog(@"JSON: %@", responseObject);
         
         NSString *result = [responseObject objectForKey:@"resultDataType"];
         
         NSLog(@"jsonArray: %@", result);
         
         if([result isEqualToString:@"1"]){
             
             [MBProgressHUD hideHUDForView:self.view animated:YES];
             [Utils simpleAlert:@"发送成功" withDelegate:self];
             
         }else{
             [MBProgressHUD hideHUDForView:self.view animated:YES];
             [Utils simpleAlert:@"发送失败"];
         }
     }
          failure:
     ^(AFHTTPRequestOperation *operation, NSError *error) {
         NSLog(@"Error: %@", error);
     }];
    
}

- (void)takePhoto {
    
    UIImagePickerController *picker = [[UIImagePickerController alloc] init];
    picker.delegate = self;
    picker.sourceType = UIImagePickerControllerSourceTypeCamera;
    picker.cameraCaptureMode = UIImagePickerControllerCameraCaptureModePhoto;
    picker.allowsEditing = YES;
    picker.showsCameraControls = YES;

    
    
    NSArray *sourceTypes =
    [UIImagePickerController availableMediaTypesForSourceType:picker.sourceType];
    if (![sourceTypes containsObject:(NSString *)kUTTypeMovie ]){
        NSLog(@"Can't save videos");
    }
    [self presentViewController:picker animated:YES completion:nil];
}

- (void)takeAlbum {
    
    UIImagePickerController *picker = [[UIImagePickerController alloc] init];
    picker.delegate = self;
    picker.allowsEditing = YES;
    picker.sourceType = UIImagePickerControllerSourceTypeSavedPhotosAlbum;
    
    
    NSArray *sourceTypes =
    [UIImagePickerController availableMediaTypesForSourceType:picker.sourceType];
    if (![sourceTypes containsObject:(NSString *)kUTTypeMovie ]){
        NSLog(@"Can't save videos");
    }
    
    [self presentViewController:picker animated:YES completion:nil];
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info
{
    NSString *mediaType = [info objectForKey:UIImagePickerControllerMediaType];
    if ([mediaType isEqualToString:(NSString *)kUTTypeImage]){
        
        if (picker.sourceType==UIImagePickerControllerSourceTypeCamera) {
            [self pickVideoWithInfo:info type:1];
        }else{
            [self pickVideoWithInfo:info type:0];
        }
    }
    [picker dismissViewControllerAnimated:YES completion:nil];
}
- (void)pickVideoWithInfo:(NSDictionary *)info type:(int)type
{
    
    
    UIImage *img = [info objectForKey:UIImagePickerControllerEditedImage];
    UIImage *sThumbnail = [img scaleToSize:CGSizeMake(500, 500)];
    selectedImageView.image = img;
    
    
    //NSData* imageData = UIImagePNGRepresentation(img);
    NSData* smallData = UIImageJPEGRepresentation(sThumbnail, 1);
    _savepath = [NSTemporaryDirectory() stringByAppendingString:@"/userdefine.jpg"];
    [smallData writeToFile:_savepath atomically:YES];
    NSLog(@"data:%@",smallData);
    
    if (type==1) {
        UIImageWriteToSavedPhotosAlbum (img, nil, nil, nil);
    }
}
#pragma mark Picker Data Source Methods

- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView {
    
    return 1;
}

- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component {
    
    return [_topicTypeArray count];
}

#pragma mark Picker Delegate Methods
- (NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component {
    
    return [_topicTypeArray objectAtIndex:row];
}

- (void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component{
    
    self.topicType.text = [_topicTypeArray objectAtIndex:row];
}
@end
