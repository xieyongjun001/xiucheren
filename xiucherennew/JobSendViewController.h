//
//  JobSendViewController.h
//  xiucherennew
//
//  Created by harry on 13-11-17.
//  Copyright (c) 2013年 harry. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface JobSendViewController : UIViewController<UIAlertViewDelegate>
@property (weak, nonatomic) IBOutlet UITextField *txtTitle;
@property (weak, nonatomic) IBOutlet UITextView *txtContent;

- (IBAction)clickCancel:(id)sender;
- (IBAction)clickSend:(id)sender;
@end
