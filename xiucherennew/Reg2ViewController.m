//
//  Reg2ViewController.m
//  xiucherennew
//
//  Created by harry on 13-11-9.
//  Copyright (c) 2013年 harry. All rights reserved.
//

#import "Reg2ViewController.h"
#import "Reg3ViewController.h"

@interface Reg2ViewController ()
@property (strong, nonatomic) UIScrollView *sclview;
@property (strong, nonatomic) NSArray *genderarr;
@property (strong, nonatomic) NSArray *gendervaluearr;
@property (strong, nonatomic) NSString *gendervalue;
@property (strong, nonatomic) NSArray *bloodarr;

@property (strong, nonatomic) UIPickerView *gendarpicker;
@property (strong, nonatomic) UIPickerView *bloodpicker;
@property (strong, nonatomic) UIDatePicker *birthpicker;
@end

@implementation Reg2ViewController
@synthesize sclview;
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    _genderarr = @[@"男",@"女"];
    _gendervaluearr = @[@"M",@"W"];
    _bloodarr = @[@"A",@"B",@"AB",@"O"];
    sclview = [[UIScrollView alloc] initWithFrame:CGRectMake(0, 60, 320, 490)];
    sclview.contentSize = CGSizeMake(320, 1080);
    sclview.backgroundColor = [UIColor clearColor];
    [self.view addSubview:sclview];
    
    UIImage *box1img = [UIImage imageNamed:@"reg2_box1"];
    UIImageView *box1imgv = [[UIImageView alloc] initWithFrame:CGRectMake(8, 20, 305, 90)];
    box1imgv.contentMode = UIViewContentModeScaleToFill;
    box1imgv.image = box1img;
    [sclview addSubview:box1imgv];
    
    UIImage *box2img = [UIImage imageNamed:@"reg2_box2"];
    UIImageView *box2imgv = [[UIImageView alloc] initWithFrame:CGRectMake(8, 115, 305, 405)];
    box2imgv.contentMode = UIViewContentModeScaleToFill;
    box2imgv.image = box2img;
    [sclview addSubview:box2imgv];
    
    UIImage *box3img = [UIImage imageNamed:@"reg2_box3"];
    UIImageView *box3imgv = [[UIImageView alloc] initWithFrame:CGRectMake(8, 525, 305, 270)];
    box3imgv.contentMode = UIViewContentModeScaleToFill;
    box3imgv.image = box3img;
    [sclview addSubview:box3imgv];
    
    UIButton *nextbt = [[UIButton alloc] initWithFrame:CGRectMake(7, 800, 305, 45)];
    [nextbt setBackgroundImage:[UIImage imageNamed:@"reg1_next"] forState:UIControlStateNormal];
    [nextbt addTarget:self action:@selector(clickNext:) forControlEvents:UIControlEventTouchUpInside];
    [sclview addSubview:nextbt];
    
    _gendarpicker = [[UIPickerView alloc] initWithFrame:CGRectMake(0, 0, 320, 216)];
    _gendarpicker.delegate = self;
    _gendarpicker.dataSource = self;
    _gendarpicker.showsSelectionIndicator = YES;
    _gendarpicker.tag = 101;
    //[self.view addSubview:_gendarpicker];
    

    _birthpicker = [[UIDatePicker alloc] initWithFrame:CGRectMake(0, 0, 295, 220)];
    _birthpicker.datePickerMode = UIDatePickerModeDate;
    [_birthpicker addTarget:self
                   action:@selector(dateSelected:)
         forControlEvents:UIControlEventValueChanged];
    //[self.view addSubview:_birthpicker];

    _bloodpicker = [[UIPickerView alloc] initWithFrame:CGRectMake(0, 0, 320, 216)];
    _bloodpicker.delegate = self;
    _bloodpicker.dataSource = self;
    _bloodpicker.showsSelectionIndicator = YES;
    _bloodpicker.tag = 103;
    //[self.view addSubview:_bloodpicker];
    
    float fromx = 125;
    float w = 175;
    float h = 28;
    //int style = UITextBorderStyleRoundedRect;
    int style = UITextBorderStyleNone;
    txtpass = [[UITextField alloc] initWithFrame:CGRectMake(fromx,30,w,h)];
    txtpass.borderStyle = style;
    txtpass.returnKeyType = UIReturnKeyNext;
    txtpass.secureTextEntry = YES;
    [sclview addSubview:txtpass];
    
    txtrepass = [[UITextField alloc] initWithFrame:CGRectMake(fromx,75,w,h)];
    txtrepass.borderStyle = style;
    txtrepass.returnKeyType = UIReturnKeyNext;
    txtrepass.secureTextEntry = YES;
    [sclview addSubview:txtrepass];
    
    txtrealname = [[UITextField alloc] initWithFrame:CGRectMake(fromx,123,w,h)];
    txtrealname.borderStyle = style;
    txtrealname.returnKeyType = UIReturnKeyNext;
    [sclview addSubview:txtrealname];
    
    txtage = [[UITextField alloc] initWithFrame:CGRectMake(fromx,168,w,h)];
    txtage.borderStyle = style;
    txtage.returnKeyType = UIReturnKeyNext;
    txtage.keyboardType = UIKeyboardTypeNumberPad;
    [sclview addSubview:txtage];
    
    txtidno = [[UITextField alloc] initWithFrame:CGRectMake(fromx,213,w,h)];
    txtidno.borderStyle = style;
    txtidno.returnKeyType = UIReturnKeyNext;
    [sclview addSubview:txtidno];
    
    txtgender = [[UITextField alloc] initWithFrame:CGRectMake(fromx,258,w,h)];
    txtgender.borderStyle = style;
    txtgender.tag = 101;
    txtgender.inputView = _gendarpicker;
    [sclview addSubview:txtgender];
    
    txtbirth = [[UITextField alloc] initWithFrame:CGRectMake(fromx,303,w,h)];
    txtbirth.borderStyle = style;
    txtbirth.tag = 102;
    txtbirth.inputView = _birthpicker;
    [sclview addSubview:txtbirth];
    
    txtblood = [[UITextField alloc] initWithFrame:CGRectMake(fromx,348,w,h)];
    txtblood.borderStyle = style;
    txtblood.tag = 103;
    txtblood.inputView = _bloodpicker;
    [sclview addSubview:txtblood];
    
    
    txtqq = [[UITextField alloc] initWithFrame:CGRectMake(fromx,393,w,h)];
    txtqq.borderStyle = style;
    txtqq.returnKeyType = UIReturnKeyNext;
    txtqq.keyboardType = UIKeyboardTypeNumberPad;
    [sclview addSubview:txtqq];
    
    txtmobile = [[UITextField alloc] initWithFrame:CGRectMake(fromx,438,w,h)];
    txtmobile.borderStyle = style;
    txtmobile.returnKeyType = UIReturnKeyNext;
    txtmobile.keyboardType = UIKeyboardTypeNumberPad;
    [sclview addSubview:txtmobile];
    
    txtemail = [[UITextField alloc] initWithFrame:CGRectMake(fromx,483,w,h)];
    txtemail.borderStyle = style;
    txtemail.returnKeyType = UIReturnKeyNext;
    txtemail.keyboardType = UIKeyboardTypeEmailAddress;
    [sclview addSubview:txtemail];
    
    txtcity = [[UITextField alloc] initWithFrame:CGRectMake(fromx,533,w,h)];
    txtcity.borderStyle = style;
    txtcity.returnKeyType = UIReturnKeyNext;
    [sclview addSubview:txtcity];
    
    
    txtaddress = [[UITextField alloc] initWithFrame:CGRectMake(fromx,580,w,h)];
    txtaddress.borderStyle = style;
    txtaddress.returnKeyType = UIReturnKeyNext;
    [sclview addSubview:txtaddress];
    
    txtzipcode = [[UITextField alloc] initWithFrame:CGRectMake(fromx,625,w,h)];
    txtzipcode.borderStyle = style;
    txtzipcode.keyboardType = UIKeyboardTypeNumberPad;
    txtzipcode.returnKeyType = UIReturnKeyNext;
    [sclview addSubview:txtzipcode];
    
    
    txtschool = [[UITextField alloc] initWithFrame:CGRectMake(fromx,673,w,h)];
    txtschool.borderStyle = style;
    txtschool.returnKeyType = UIReturnKeyNext;
    [sclview addSubview:txtschool];
    
    txtworkexp1 = [[UITextField alloc] initWithFrame:CGRectMake(fromx,712,w,h)];
    txtworkexp1.borderStyle = style;
    txtworkexp1.returnKeyType = UIReturnKeyNext;
    [sclview addSubview:txtworkexp1];
    
    txtworkexp2 = [[UITextField alloc] initWithFrame:CGRectMake(fromx,757,w,h)];
    txtworkexp2.borderStyle = style;
    txtworkexp2.returnKeyType = UIReturnKeyNext;
    [sclview addSubview:txtworkexp2];
    
  
  
    
    
//    txt = [[UITextField alloc] initWithFrame:CGRectMake(fromx,75,w,h)];
//    txtrepass.borderStyle = UITextBorderStyleRoundedRect;
//    txtrepass.secureTextEntry = YES;
//    [sclview addSubview:txtrepass];
    
    
    // Do any additional setup after loading the view from its nib.
}




//页面加载前调用的方法,注册两个通知：一个是键盘弹出来的通知，另外一个是键盘隐藏的通知，不同的通知调用不同的方法进行处理
-(void) viewWillAppear:(BOOL)animated{
    //键盘弹起的通知
    [[NSNotificationCenter defaultCenter]
        addObserver:self
        selector:@selector(keyboardDidShow:)
        name:UIKeyboardDidShowNotification
        object:self.view.window];
    
    //键盘隐藏的通知
    [[NSNotificationCenter defaultCenter]
        addObserver:self
        selector:@selector(keyboardDidHide:)
        name:UIKeyboardDidHideNotification
        object:nil];
}

//页面消失前取消通知
-(void)viewWillDisappear:(BOOL)animated{
    [[NSNotificationCenter defaultCenter]
     removeObserver:self
     name:UIKeyboardDidShowNotification
     object:nil];
    
    [[NSNotificationCenter defaultCenter]
     removeObserver:self
     name:UIKeyboardDidHideNotification
     object:nil];
}

- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField{
    if (textField.tag==101 || textField.tag==103) {
        return NO;
    }
    return YES;
}


-(void)textFieldDidBeginEditing:(UITextField *)textFieldView{
    currentTextfield = textFieldView;
}


-(BOOL)textFieldShouldReturn:(UITextField *)textFieldView{
    [textFieldView resignFirstResponder];
    return NO;
}

-(void)textFieldDidEndEditing:(UITextField *)textFieldView{
    currentTextfield = nil;
}

//键盘弹起后处理scrollView的高度使得textfield可见
-(void)keyboardDidShow:(NSNotification *)notification{
    if (keyboardIsShown) {
        return;
    }
    NSDictionary * info = [notification userInfo];
    NSValue *avalue = [info objectForKey:UIKeyboardFrameEndUserInfoKey];
    CGRect keyboardRect = [self.view convertRect:[avalue CGRectValue] fromView:nil];
    CGRect viewFrame = [sclview frame];
    viewFrame.size.height -= keyboardRect.size.height;
    sclview.frame = viewFrame;
    CGRect textFieldRect = [currentTextfield frame];
    [sclview scrollRectToVisible:textFieldRect animated:YES];
    keyboardIsShown = YES;
}

//键盘隐藏后处理scrollview的高度，使其还原为本来的高度
-(void)keyboardDidHide:(NSNotification *)notification{
    NSDictionary *info = [notification userInfo];
    NSValue *avalue = [info objectForKey:UIKeyboardFrameEndUserInfoKey];
    CGRect keyboardRect = [self.view convertRect:[avalue CGRectValue] fromView:nil];
    CGRect viewFrame = [sclview frame];
    viewFrame.size.height += keyboardRect.size.height;
    sclview.frame = viewFrame;
    keyboardIsShown = NO;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)clickNext:(id)sender {
  
    
    if ([txtpass.text length]==0 || [txtrepass.text length]==0) {
        [Utils simpleAlert:@"请填写密码"];
        return ;
    }
    
    if (![txtpass.text isEqualToString:txtrepass.text]) {
        [Utils simpleAlert:@"密码不一致"];
        return ;
    }
    
    if ([txtrealname.text length]==0) {
        [Utils simpleAlert:@"请填写真实姓名"];
        return ;
    }
    
    if ([txtage.text length]==0) {
        [Utils simpleAlert:@"请填写年龄"];
        return ;
    }
    
    if ([txtidno.text length]==0) {
        [Utils simpleAlert:@"请填写身份证号码"];
        return ;
    }
    
    if ([txtgender.text length]==0) {
        [Utils simpleAlert:@"请选择性别"];
        return ;
    }
    
    if ([txtbirth.text length]==0) {
        [Utils simpleAlert:@"请选择生日"];
        return ;
    }
    
    if ([txtblood.text length]==0) {
        [Utils simpleAlert:@"请选择血型"];
        return ;
    }
    
    
    if ([txtqq.text length]==0) {
        [Utils simpleAlert:@"请选择QQ号码"];
        return ;
    }
    
    if ([txtmobile.text length]==0) {
        [Utils simpleAlert:@"请填写手机号码"];
        return ;
    }
    
    if ([txtemail.text length]==0) {
        [Utils simpleAlert:@"请填写邮箱"];
        return ;
    }
    
    if ([txtcity.text length]==0) {
        [Utils simpleAlert:@"请填写城市"];
        return ;
    }
    
    if ([txtaddress.text length]==0) {
        [Utils simpleAlert:@"请填写地址"];
        return ;
    }
    
    if ([txtzipcode.text length]==0) {
        [Utils simpleAlert:@"请填写邮编"];
        return ;
    }
    
    if ([txtschool.text length]==0) {
        [Utils simpleAlert:@"请填写毕业院校"];
        return ;
    }
    
    if ([txtworkexp1.text length]==0) {
        [Utils simpleAlert:@"请填写工作经历1"];
        return ;
    }
    
    if ([txtworkexp2.text length]==0) {
        [Utils simpleAlert:@"请填写工作经历1"];
        return ;
    }
     
    
//    /registerAutoStepTwo/{password}/{userName}/{userAge}/{idCardNo}/{gender}/{birthday}/{blood}/{QQ}/{mobile}/{mail}/{city}/{address}/{zipcode}/{school}/{wordExp}/{orgGuid}
    
    NSString *string = [NSString stringWithFormat:@"%@%@%@/%@/%@/%@/%@/%@/%@/%@/%@/%@/%@/%@/%@/%@/%@/",
                        WEBSERVICE_URL,
                        WEBSERVICE_Reg2,
                        txtpass.text,
                        [txtrealname.text urlencode],
                        txtage.text,
                        txtidno.text,
                        _gendervalue,
                        txtbirth.text,
                        txtblood.text,
                        txtqq.text,
                        txtmobile.text,
                        txtemail.text,
                        [txtcity.text urlencode],
                        [txtaddress.text urlencode],
                        txtzipcode.text,
                        [txtschool.text urlencode],
                        [[NSString stringWithFormat:@"%@|||%@",txtworkexp1.text,txtworkexp2.text] urlencode]];
    NSLog(@"string: %@", string);
    Reg3ViewController *rvc = [[Reg3ViewController alloc] initWithNibName:@"Reg3ViewController" bundle:nil];
    rvc.querystring = string;
    [self.navigationController pushViewController:rvc animated:YES];
    
}

- (IBAction)clickBack:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}


- (void)dateSelected:(id)sender{
    UIDatePicker *dp = (UIDatePicker *)sender;
    

    
	NSDateFormatter *df = [[NSDateFormatter alloc] init];
	[df setDateFormat:@"yyyy-MM-dd"];
	//NSLog(@"%@", [df stringFromDate:dp.date]);
    
    txtbirth.text =[df stringFromDate:dp.date];
}


#pragma mark - picker
- (void)pickerView:(UIPickerView *)pickerView didSelectRow: (NSInteger)row inComponent:(NSInteger)component {
    // Handle the selection
    if(row==0){
        if(pickerView.tag==101){
            _gendervalue =  @"";
            txtgender.text =  @"";
        }
        if(pickerView.tag==103){
            txtblood.text =  @"";
        }
        
    }else{
        if(pickerView.tag==101){
            _gendervalue = [_gendervaluearr objectAtIndex:row-1];
            txtgender.text = [_genderarr objectAtIndex:row-1];
        }
        if(pickerView.tag==103){
           txtblood.text = [_bloodarr objectAtIndex:row-1];
        }
    }
}

// tell the picker how many rows are available for a given component
- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component {
    if(pickerView.tag==101){
        return [_genderarr count]+1;
    }
    if(pickerView.tag==103){
        return [_bloodarr count]+1;
    }
    
    return 0;
}

// tell the picker how many components it will have
- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView {
    return 1;
}

// tell the picker the title for a given component
- (NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component {
    if(row>0){
        if(pickerView.tag==101){
            return [_genderarr objectAtIndex:row-1];
        }
        if(pickerView.tag==103){
            return [_bloodarr objectAtIndex:row-1];
        }
    }
    return @"请选择";
}

// tell the picker the width of each row for a given component
- (CGFloat)pickerView:(UIPickerView *)pickerView widthForComponent:(NSInteger)component {
    int sectionWidth = 300;
    
    return sectionWidth;
}

@end
