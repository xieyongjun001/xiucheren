//
//  EditUserViewController.m
//  xiucherennew
//
//  Created by harry on 13-11-27.
//  Copyright (c) 2013年 harry. All rights reserved.
//

#import "EditUserViewController.h"

@interface EditUserViewController ()
@property (strong, nonatomic) UIScrollView *sclview;
@property (strong, nonatomic) NSArray *genderarr;
@property (strong, nonatomic) NSArray *gendervaluearr;
@property (strong, nonatomic) NSString *gendervalue;
@property (strong, nonatomic) NSArray *bloodarr;

@property (strong, nonatomic) UIPickerView *gendarpicker;
@property (strong, nonatomic) UIPickerView *bloodpicker;
@property (strong, nonatomic) UIDatePicker *birthpicker;
@end

@implementation EditUserViewController
@synthesize sclview;
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)resignKeyBoardInView
{
    for (UIView *v in sclview.subviews) {

        
        if ([v isKindOfClass:[UITextView class]] || [v isKindOfClass:[UITextField class]]) {
            [v resignFirstResponder];
        }
    }
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    _isedit = 0;
    
    _genderarr = @[@"男",@"女"];
    _gendervaluearr = @[@"M",@"W"];
    _bloodarr = @[@"A",@"B",@"AB",@"O"];
    sclview = [[UIScrollView alloc] initWithFrame:CGRectMake(0, 60, 320, 490)];
    sclview.contentSize = CGSizeMake(320, 550);
    sclview.backgroundColor = [UIColor clearColor];
    [self.view addSubview:sclview];
    
    UITapGestureRecognizer *tapGr = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(resignKeyBoardInView)];
    tapGr.cancelsTouchesInView = NO;
    [sclview addGestureRecognizer:tapGr];
    
    UIImage *box1img = [UIImage imageNamed:@"mydt_txtbg"];
    UIImageView *box1imgv = [[UIImageView alloc] initWithFrame:CGRectMake(8, 20, 305, 445)];
    box1imgv.contentMode = UIViewContentModeScaleToFill;
    box1imgv.image = box1img;
    [sclview addSubview:box1imgv];
    
    
    _gendarpicker = [[UIPickerView alloc] initWithFrame:CGRectMake(0, 0, 320, 216)];
    _gendarpicker.delegate = self;
    _gendarpicker.dataSource = self;
    _gendarpicker.showsSelectionIndicator = YES;
    _gendarpicker.tag = 101;
    //[self.view addSubview:_gendarpicker];
    
    
    _birthpicker = [[UIDatePicker alloc] initWithFrame:CGRectMake(0, 0, 295, 220)];
    _birthpicker.datePickerMode = UIDatePickerModeDate;
    [_birthpicker addTarget:self
                     action:@selector(dateSelected:)
           forControlEvents:UIControlEventValueChanged];
    //[self.view addSubview:_birthpicker];
    
    _bloodpicker = [[UIPickerView alloc] initWithFrame:CGRectMake(0, 0, 320, 216)];
    _bloodpicker.delegate = self;
    _bloodpicker.dataSource = self;
    _bloodpicker.showsSelectionIndicator = YES;
    _bloodpicker.tag = 103;
    //[self.view addSubview:_bloodpicker];
    
    float fromx = 85;
    float w = 215;
    float h = 28;
    //int style = UITextBorderStyleRoundedRect;
    int style = UITextBorderStyleNone;
    txtloginname = [[UITextField alloc] initWithFrame:CGRectMake(fromx,20,w,h)];
    txtloginname.borderStyle = style;
    txtloginname.delegate = self;
    [sclview addSubview:txtloginname];
    
    float gap=26;
    float fromy=61;
    
    txtrealname = [[UITextField alloc] initWithFrame:CGRectMake(fromx,fromy,w,h)];
    txtrealname.borderStyle = style;
    txtrealname.delegate = self;
    [sclview addSubview:txtrealname];
    
    txtage = [[UITextField alloc] initWithFrame:CGRectMake(fromx,fromy+1*gap,w,h)];
    txtage.borderStyle = style;
    txtage.delegate = self;
    txtage.keyboardType = UIKeyboardTypeNumberPad;
    [sclview addSubview:txtage];
    
    txtidno = [[UITextField alloc] initWithFrame:CGRectMake(fromx,fromy+2*gap,w,h)];
    txtidno.borderStyle = style;
    txtidno.delegate = self;
    [sclview addSubview:txtidno];
    
    txtgender = [[UITextField alloc] initWithFrame:CGRectMake(fromx,fromy+3*gap,w,h)];
    txtgender.borderStyle = style;
    txtgender.tag = 101;
    txtgender.inputView = _gendarpicker;
    txtgender.delegate = self;
    [sclview addSubview:txtgender];
    
    txtbirth = [[UITextField alloc] initWithFrame:CGRectMake(fromx,fromy+4*gap,w,h)];
    txtbirth.borderStyle = style;
    txtbirth.tag = 102;
    txtbirth.inputView = _birthpicker;
    txtbirth.delegate = self;
    [sclview addSubview:txtbirth];
    
    txtblood = [[UITextField alloc] initWithFrame:CGRectMake(fromx,fromy+5*gap,w,h)];
    txtblood.borderStyle = style;
    txtblood.tag = 103;
    txtblood.inputView = _bloodpicker;
    txtblood.delegate = self;
    [sclview addSubview:txtblood];
    
    
    txtqq = [[UITextField alloc] initWithFrame:CGRectMake(fromx,fromy+6*gap,w,h)];
    txtqq.borderStyle = style;
    txtqq.delegate = self;
    txtqq.keyboardType = UIKeyboardTypeNumberPad;
    [sclview addSubview:txtqq];
    
    txtmobile = [[UITextField alloc] initWithFrame:CGRectMake(fromx,fromy+7*gap,w,h)];
    txtmobile.borderStyle = style;
    txtmobile.delegate = self;
    txtmobile.keyboardType = UIKeyboardTypeNumberPad;
    [sclview addSubview:txtmobile];
    
    txtemail = [[UITextField alloc] initWithFrame:CGRectMake(fromx,fromy+8*gap,w,h)];
    txtemail.borderStyle = style;
    txtemail.delegate = self;
    txtemail.keyboardType = UIKeyboardTypeEmailAddress;
    [sclview addSubview:txtemail];
    
    fromy=309;
    
    txtcity = [[UITextField alloc] initWithFrame:CGRectMake(fromx,fromy,w,h)];
    txtcity.borderStyle = style;
    txtcity.delegate = self;
    [sclview addSubview:txtcity];
    
    
    txtaddress = [[UITextField alloc] initWithFrame:CGRectMake(fromx,fromy+gap,w,h)];
    txtaddress.borderStyle = style;
    txtaddress.delegate = self;
    [sclview addSubview:txtaddress];
    
    txtzipcode = [[UITextField alloc] initWithFrame:CGRectMake(fromx,fromy+2*gap,w,h)];
    txtzipcode.borderStyle = style;
    txtzipcode.keyboardType = UIKeyboardTypeNumberPad;
    txtzipcode.delegate = self;
    [sclview addSubview:txtzipcode];
    
    
    txtschool = [[UITextField alloc] initWithFrame:CGRectMake(fromx,fromy+3*gap,w,h)];
    txtschool.borderStyle = style;
    txtschool.delegate = self;
    [sclview addSubview:txtschool];
    
    txtworkexp1 = [[UITextField alloc] initWithFrame:CGRectMake(fromx,fromy+4*gap,w,h)];
    txtworkexp1.borderStyle = style;
    txtworkexp1.delegate = self;
    [sclview addSubview:txtworkexp1];
    
    txtworkexp2 = [[UITextField alloc] initWithFrame:CGRectMake(fromx,fromy+5*gap,w,h)];
    txtworkexp2.borderStyle = style;
    txtworkexp2.delegate = self;
    [sclview addSubview:txtworkexp2];
    
    
    
    
    
    //    txt = [[UITextField alloc] initWithFrame:CGRectMake(fromx,75,w,h)];
    //    txtrepass.borderStyle = UITextBorderStyleRoundedRect;
    //    txtrepass.secureTextEntry = YES;
    //    [sclview addSubview:txtrepass];
    
    
    // Do any additional setup after loading the view from its nib.
}

- (void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    NSString *string = [NSString stringWithFormat:@"%@%@%@",WEBSERVICE_URL,WEBSERVICE_GetUserinfo,[AppDelegate getAppDelegate].userCode];
    
    [manager GET:string parameters:nil success:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        NSLog(@"JSON: %@", responseObject);
        
        NSString *result = [responseObject objectForKey:@"resultDataType"];
        
        NSLog(@"jsonArray: %@", result);
        
        if([result isEqualToString:@"1"]){
            NSArray *arr = [responseObject objectForKey:@"resultList"];
            NSDictionary *dic = [arr objectAtIndex:0];
            NSString *gstring =[dic objectForKey:@"auserGender"];
            _gendervalue = [dic objectForKey:@"auserGender"];
            int gid = [_gendervaluearr indexOfObject:gstring];
            txtgender.text = [_genderarr objectAtIndex:gid];
            txtaddress.text = [dic objectForKey:@"auserAddress"];
            txtage.text = [dic objectForKey:@"auserAge"];
            txtbirth.text = [[dic objectForKey:@"auserBirthday"] isKindOfClass:[NSNull class]]?@"":[dic objectForKey:@"auserBirthday"];
            txtblood.text = [dic objectForKey:@"auserBloodType"];
            txtcity.text = [dic objectForKey:@"auserCity"];
            txtemail.text = [dic objectForKey:@"auserMail"];
            
            txtidno.text = [dic objectForKey:@"auserIdCode"];
            txtloginname.text = [dic objectForKey:@"auserCode"];
            txtmobile.text = [dic objectForKey:@"auserMobile"];
            txtqq.text = [dic objectForKey:@"auserQq"];
            txtrealname.text = [dic objectForKey:@"auserName"];
            txtschool.text = [dic objectForKey:@"auserGraduateSchool"];
            
            NSString *exp = [dic objectForKey:@"auserWorkExperience"];
            NSArray *exparr = [exp componentsSeparatedByString:@"|||"];
            if ([exparr count]==1) {
                txtworkexp1.text = [exparr objectAtIndex:0];
            }
            if ([exparr count]==2) {
                txtworkexp1.text = [exparr objectAtIndex:0];
                txtworkexp2.text = [exparr objectAtIndex:1];
            }
           
            txtzipcode.text = [dic objectForKey:@"auserZipcode"];
            
        }
        
        [MBProgressHUD hideHUDForView:self.view animated:YES];
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"Error: %@", error);
        [MBProgressHUD hideHUDForView:self.view animated:YES];
    }];
}





//页面加载前调用的方法,注册两个通知：一个是键盘弹出来的通知，另外一个是键盘隐藏的通知，不同的通知调用不同的方法进行处理
-(void) viewWillAppear:(BOOL)animated{
    //键盘弹起的通知
    [[NSNotificationCenter defaultCenter]
     addObserver:self
     selector:@selector(keyboardDidShow:)
     name:UIKeyboardDidShowNotification
     object:self.view.window];
    
    //键盘隐藏的通知
    [[NSNotificationCenter defaultCenter]
     addObserver:self
     selector:@selector(keyboardDidHide:)
     name:UIKeyboardDidHideNotification
     object:nil];
}

//页面消失前取消通知
-(void)viewWillDisappear:(BOOL)animated{
    [[NSNotificationCenter defaultCenter]
     removeObserver:self
     name:UIKeyboardDidShowNotification
     object:nil];
    
    [[NSNotificationCenter defaultCenter]
     removeObserver:self
     name:UIKeyboardDidHideNotification
     object:nil];
}

- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField{
    if (!_isedit) {
        return NO;
    }
    
    return YES;
}


-(void)textFieldDidBeginEditing:(UITextField *)textFieldView{
    currentTextfield = textFieldView;
}


-(BOOL)textFieldShouldReturn:(UITextField *)textFieldView{
    [textFieldView resignFirstResponder];
    return NO;
}

-(void)textFieldDidEndEditing:(UITextField *)textFieldView{
    currentTextfield = nil;
}

//键盘弹起后处理scrollView的高度使得textfield可见
-(void)keyboardDidShow:(NSNotification *)notification{
    if (keyboardIsShown) {
        return;
    }
    NSDictionary * info = [notification userInfo];
    NSValue *avalue = [info objectForKey:UIKeyboardFrameEndUserInfoKey];
    CGRect keyboardRect = [self.view convertRect:[avalue CGRectValue] fromView:nil];
    CGRect viewFrame = [sclview frame];
    viewFrame.size.height -= keyboardRect.size.height;
    sclview.frame = viewFrame;
    CGRect textFieldRect = [currentTextfield frame];
    [sclview scrollRectToVisible:textFieldRect animated:YES];
    keyboardIsShown = YES;
}

//键盘隐藏后处理scrollview的高度，使其还原为本来的高度
-(void)keyboardDidHide:(NSNotification *)notification{
    NSDictionary *info = [notification userInfo];
    NSValue *avalue = [info objectForKey:UIKeyboardFrameEndUserInfoKey];
    CGRect keyboardRect = [self.view convertRect:[avalue CGRectValue] fromView:nil];
    CGRect viewFrame = [sclview frame];
    viewFrame.size.height += keyboardRect.size.height;
    sclview.frame = viewFrame;
    keyboardIsShown = NO;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


- (IBAction)clickBack:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)clickEdit:(id)sender {
    
    if (!_isedit) {
        [_editbt setImage:[UIImage imageNamed:@"jobsender_save"] forState:UIControlStateNormal];
    }else{
        if ([txtrealname.text length]==0) {
            [Utils simpleAlert:@"请填写真实姓名"];
            return ;
        }
        
        if ([txtage.text length]==0) {
            [Utils simpleAlert:@"请填写年龄"];
            return ;
        }
        
        if ([txtidno.text length]==0) {
            [Utils simpleAlert:@"请填写身份证号码"];
            return ;
        }
        
        if ([txtgender.text length]==0) {
            [Utils simpleAlert:@"请选择性别"];
            return ;
        }
        
        if ([txtbirth.text length]==0) {
            [Utils simpleAlert:@"请选择生日"];
            return ;
        }
        
        if ([txtblood.text length]==0) {
            [Utils simpleAlert:@"请选择血型"];
            return ;
        }
        
        
        if ([txtqq.text length]==0) {
            [Utils simpleAlert:@"请选择QQ号码"];
            return ;
        }
        
        if ([txtmobile.text length]==0) {
            [Utils simpleAlert:@"请填写手机号码"];
            return ;
        }
        
        if ([txtemail.text length]==0) {
            [Utils simpleAlert:@"请填写邮箱"];
            return ;
        }
        
        if ([txtcity.text length]==0) {
            [Utils simpleAlert:@"请填写城市"];
            return ;
        }
        
        if ([txtaddress.text length]==0) {
            [Utils simpleAlert:@"请填写地址"];
            return ;
        }
        
        if ([txtzipcode.text length]==0) {
            [Utils simpleAlert:@"请填写邮编"];
            return ;
        }
        
        if ([txtschool.text length]==0) {
            [Utils simpleAlert:@"请填写毕业院校"];
            return ;
        }
        
        if ([txtworkexp1.text length]==0) {
            [Utils simpleAlert:@"请填写工作经历1"];
            return ;
        }
        
        if ([txtworkexp2.text length]==0) {
            [Utils simpleAlert:@"请填写工作经历2"];
            return ;
        }
        
        
        //    /registerAutoStepTwo/{password}/{userName}/{userAge}/{idCardNo}/{gender}/{birthday}/{blood}/{QQ}/{mobile}/{mail}/{city}/{address}/{zipcode}/{school}/{wordExp}/{orgGuid}
        NSDictionary *useridc = [AppDelegate getAppDelegate].userDict;
        NSString *auserGuid = [useridc objectForKey:@"auserGuid"];
        NSDictionary *dict = @{@"auserGuid":auserGuid,@"auserName":txtrealname.text,@"auserNickName":@"",@"auserBirthday":txtbirth.text,@"auserGender":_gendervalue,@"auserAge":txtage.text,@"auserPy":@"",@"auserPyBref":@"",@"auserBloodType":txtblood.text,@"auserQq":txtqq.text,@"auserMobile":txtmobile.text,@"auserPhone":@"",@"auserMail":txtemail.text,@"auserCity":txtcity.text,@"auserGraduateSchool":txtschool.text,@"auserWorkExperience":[NSString stringWithFormat:@"%@|||%@",txtworkexp1.text,txtworkexp2.text],@"auserAddress":txtaddress.text,@"auserZipcode":txtzipcode.text,@"auserIdType":@"",@"auserIdCode":txtidno.text};
        
        NSString *output = [dict JSONRepresentation];
        
        [MBProgressHUD showHUDAddedTo:self.view animated:YES];
        AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
        NSString *string = [NSString stringWithFormat:@"%@%@%@/%@",WEBSERVICE_URL,WEBSERVICE_UpdateUserinfo,[AppDelegate getAppDelegate].userCode,[output urlencode]];
        
        [manager POST:string parameters:nil success:^(AFHTTPRequestOperation *operation, id responseObject) {
            
            NSLog(@"JSON: %@", responseObject);
            
            NSString *result = [responseObject objectForKey:@"resultDataType"];
            
            NSLog(@"jsonArray: %@", result);
            
            if([result isEqualToString:@"1"])
            {
                [Utils simpleAlert:@"保存成功"];
                [_editbt setImage:[UIImage imageNamed:@"jobsender_edit"] forState:UIControlStateNormal];
                [self resignKeyBoardInView];
                
            }else{
                [Utils simpleAlert:[responseObject objectForKey:@"resultInfo"]];
            }
            
            
            [MBProgressHUD hideHUDForView:self.view animated:YES];
        } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
            NSLog(@"Error: %@", error);
            [MBProgressHUD hideHUDForView:self.view animated:YES];
        }];
        
    }
    _isedit = !_isedit;
    //_txtContent.editable = !_txtContent.editable;
}


- (void)dateSelected:(id)sender{
    UIDatePicker *dp = (UIDatePicker *)sender;
    
    
    
	NSDateFormatter *df = [[NSDateFormatter alloc] init];
	[df setDateFormat:@"yyyy-MM-dd"];
	//NSLog(@"%@", [df stringFromDate:dp.date]);
    
    txtbirth.text =[df stringFromDate:dp.date];
}


#pragma mark - picker
- (void)pickerView:(UIPickerView *)pickerView didSelectRow: (NSInteger)row inComponent:(NSInteger)component {
    // Handle the selection
    
    if(pickerView.tag==101){
        _gendervalue = [_gendervaluearr objectAtIndex:row];
        txtgender.text = [_genderarr objectAtIndex:row];
    }
    if(pickerView.tag==103){
        txtblood.text = [_bloodarr objectAtIndex:row];;
    }
}

// tell the picker how many rows are available for a given component
- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component {
    if(pickerView.tag==101){
        return [_genderarr count];
    }
    if(pickerView.tag==103){
        return [_bloodarr count];
    }
    
    return 0;
}

// tell the picker how many components it will have
- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView {
    return 1;
}

// tell the picker the title for a given component
- (NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component {
    if(pickerView.tag==101){
        return [_genderarr objectAtIndex:row];
    }
    if(pickerView.tag==103){
        return [_bloodarr objectAtIndex:row];
        
    }
    return @"";
}

// tell the picker the width of each row for a given component
- (CGFloat)pickerView:(UIPickerView *)pickerView widthForComponent:(NSInteger)component {
    int sectionWidth = 300;
    
    return sectionWidth;
}
@end
