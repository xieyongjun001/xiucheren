//
//  JobEditViewController.h
//  xiucherennew
//
//  Created by harry on 13-11-16.
//  Copyright (c) 2013年 harry. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface JobEditViewController : UIViewController
@property (strong, nonatomic) NSString *taskguid;

@property (weak, nonatomic) IBOutlet UIButton *btEdit;
@property (weak, nonatomic) IBOutlet UILabel *lblTitle;
@property (weak, nonatomic) IBOutlet UITextView *txtContent;
@property (weak, nonatomic) IBOutlet UIScrollView *sclUnfinish;
@property (weak, nonatomic) IBOutlet UIScrollView *sclFinish;

- (IBAction)clickEdit:(id)sender;
- (IBAction)clickBack:(id)sender;
@end
