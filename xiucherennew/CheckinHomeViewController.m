//
//  CheckinHomeViewController.m
//  xiucherennew
//
//  Created by harry on 13-11-21.
//  Copyright (c) 2013年 harry. All rights reserved.
//

#import "CheckinHomeViewController.h"
#import "CheckinPointViewController.h"

@interface CheckinHomeViewController ()

@end

@implementation CheckinHomeViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    NSDate* date = [NSDate date];
    
    NSDateFormatter* formatter = [[NSDateFormatter alloc] init];
    
    [formatter setDateFormat:@"yyyy-MM-dd"];
    
    NSString* str = [formatter stringFromDate:date];
    
    _lblTime.text = str;
    
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    NSString *string = [NSString stringWithFormat:@"%@%@%@",WEBSERVICE_URL,WEBSERVICE_IsCheckin,[AppDelegate getAppDelegate].userCode];
    NSLog(@"string: %@", string);
    [manager GET:string parameters:nil success:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        NSLog(@"JSON: %@", responseObject);
        
        NSString *result = [responseObject objectForKey:@"resultDataType"];
        
        NSLog(@"jsonArray: %@", result);
        
        if([result isEqualToString:@"1"]){
            _txtCheckinSuccess.hidden = NO;
            _imgvTick.hidden = NO;
            _btCheckin.hidden = YES;
            
        }        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"Error: %@", error);
    }];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)clickBack:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)clickCheckin:(id)sender {
    
 
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    NSString *string = [NSString stringWithFormat:@"%@%@/%@",WEBSERVICE_URL,WEBSERVICE_Checkin,[AppDelegate getAppDelegate].userCode];
    NSLog(@"string: %@", string);
    [manager POST:string parameters:nil success:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        NSLog(@"JSON: %@", responseObject);
        
        NSString *result = [responseObject objectForKey:@"resultDataType"];
        
        NSLog(@"jsonArray: %@", result);
        
        if([result isEqualToString:@"1"]){
            _txtCheckinSuccess.hidden = NO;
            _imgvTick.hidden = NO;
            _btCheckin.hidden = YES;
            
        }else{
            [Utils simpleAlert:@"签到失败"];
        }
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"Error: %@", error);
    }];

    
}

- (IBAction)clickPoint:(id)sender {
    CheckinPointViewController *svc = [[CheckinPointViewController alloc] initWithNibName:@"CheckinPointViewController" bundle:nil];
    [self.navigationController pushViewController:svc animated:YES];
}
@end
