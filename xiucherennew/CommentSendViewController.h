//
//  CommentSendViewController.h
//  xiucherennew
//
//  Created by harry on 13-12-7.
//  Copyright (c) 2013年 harry. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CommentSendViewController : UIViewController<UIAlertViewDelegate>

- (IBAction)clickCancel:(id)sender;
- (IBAction)clickSend:(id)sender;
@property (weak, nonatomic) IBOutlet GCPlaceholderTextView *txtContent;
@property (strong, nonatomic) NSString *postGuid;
@end
