//
//  SendNewsViewController.m
//  xiucherennew
//
//  Created by harry on 13-11-9.
//  Copyright (c) 2013年 harry. All rights reserved.
//

#import "SendNewsViewController.h"
#import "NSString+URLENCODE.h"
#import "MoreMemberListViewController.h"
#import "UIImage+Thumb.h"

@interface SendNewsViewController ()<UIPickerViewDataSource,UIPickerViewDelegate>{

    NSMutableArray *_newsTypeArray;
    NSString *_selectedNewsType;
}

@property (nonatomic,strong) MoreMemberListViewController *mlvc;
@property (nonatomic,strong) NSString *savepath;


@end

@implementation SendNewsViewController

@synthesize pickerView;
@synthesize newsTypeTxtFild;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    //[_txtContent becomeFirstResponder];
    _txtContent.placeholder = @"发布新鲜事";
    
    //_newsTypeArray = @[@"钣金",@"机修",@"保养",@"改装",@"玻璃",@"零售",@"批发"];
    pickerView = [[UIPickerView alloc] init];
    pickerView.backgroundColor = [UIColor lightGrayColor];
    pickerView.autoresizingMask = UIViewAutoresizingFlexibleHeight|UIViewAutoresizingFlexibleWidth;
    pickerView.dataSource = self;
    pickerView.delegate = self;
    pickerView.showsSelectionIndicator = YES;
    self.newsTypeTxtFild.inputView = pickerView;
    
    UILabel *leftLabel = [[UILabel alloc] init];
    leftLabel.frame = CGRectMake(0, 0, 10, 30);
    
    self.newsTypeTxtFild.leftView = leftLabel;
    self.newsTypeTxtFild.leftViewMode = UITextFieldViewModeAlways;
    
    UIToolbar *toolBar = [[UIToolbar alloc]initWithFrame:CGRectMake(0, 0, 320, 44)];
    UIBarButtonItem *flexSpace = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:self action:nil];
    UIBarButtonItem *rightButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"确定" style:UIBarButtonItemStylePlain target:self action:@selector(doIt)];
    toolBar.items = [NSArray arrayWithObjects:flexSpace,rightButtonItem, nil];
    
    self.newsTypeTxtFild.inputAccessoryView = toolBar;
    
    
    UIImageView *rightImageView = [[UIImageView alloc] init];
    rightImageView.frame = CGRectMake(0, 0, 30, 29);
    rightImageView.image = [UIImage imageNamed:@"reg3_downarr.png"];
    self.newsTypeTxtFild.rightView = rightImageView;
    self.newsTypeTxtFild.rightViewMode = UITextFieldViewModeAlways;
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyBoardWillShow:) name:UIKeyboardWillShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyBoardWillHide:) name:UIKeyboardWillHideNotification object:nil];
}



- (void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    [self initNewsType];
    [_txtContent becomeFirstResponder];

    
    if (!_mlvc) {
        _mlvc = [[MoreMemberListViewController alloc] initWithNibName:@"MoreMemberListViewController" bundle:nil];
        _mlvc.gosixin = 3;
    }else{
        NSArray *arr = [NSArray arrayWithArray:_mlvc.selectedUser];
        NSLog(@"%@",arr);
        NSString *dd = @"";
        for (int i=0; i<[arr count]; i++) {
            NSDictionary *udict = [arr objectAtIndex:i];
            NSString *uname = [udict objectForKey:@"auserName"];
            NSString *tm = [@"@" stringByAppendingString:uname];
            _txtContent.text = [_txtContent.text stringByReplacingOccurrencesOfString:tm withString:@""];
            
            dd = [dd stringByAppendingFormat:@"@%@ ",uname];
        }
        _txtContent.text = [NSString stringWithFormat:@"%@%@",dd,_txtContent.text];
        
    }
    
    
}

-(void)initNewsType{

    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    NSString *string = [NSString stringWithFormat:@"%@%@",WEBSERVICE_URL,WEBSERVICE_NewsType];
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    [manager GET:string parameters:nil success:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSString *result = [responseObject objectForKey:@"resultDataType"];
        
        NSLog(@"jsonArray: %@", result);
        
        if([result isEqualToString:@"1"]){
             [MBProgressHUD hideHUDForView:self.view animated:YES];
            _newsTypeArray =  [[NSMutableArray alloc] initWithArray:[responseObject objectForKey:@"resultList"]];
        }else{
            NSString *msg = [responseObject objectForKey:@"resultInfo"];
            [MBProgressHUD hideHUDForView:self.view animated:YES];
            [Utils simpleAlert:msg];
        }
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"Error: %@", error);
    }];
}
-(void)doIt
{
    if ([self.newsTypeTxtFild.text length]==0) {
        self.newsTypeTxtFild.text = [[_newsTypeArray objectAtIndex:0] objectForKey:@"osccName"];
        _selectedNewsType = [[_newsTypeArray objectAtIndex:0] objectForKey:@"osccCode"];
    }
    [self.newsTypeTxtFild resignFirstResponder];
    
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)clickCancel:(id)sender {
    
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)clickSend:(id)sender {
    if ([_txtContent.text length]==0||self.newsTypeTxtFild.text==0) {
        [Utils simpleAlert:@"请填写要类型或内容！"];
        return ;
    }
    
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    
    if (_savepath) {
        AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
        NSURL *filePath = [NSURL fileURLWithPath:_savepath];
        [manager POST:WEBSERVICE_UploadPhoto parameters:nil constructingBodyWithBlock:^(id<AFMultipartFormData> formData) {
            [formData appendPartWithFileURL:filePath name:@"userdefine" error:nil];
        } success:^(AFHTTPRequestOperation *operation, id responseObject) {
            NSLog(@"Success: %@", responseObject);
            
            NSArray *resultarr = [responseObject objectForKey:@"resultList"];
            [self submit:resultarr];
            
        } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
            NSLog(@"Error: %@", error);
        }];
    }else{
         [self submit:nil];
    }
   
}

- (void)submit:(NSArray *)dic{
    
    //("/publishPostInfoNew/{userCode}/{postType}/{wordContent}")
    NSString *string = [NSString stringWithFormat:@"%@%@%@/%@/%@/%@",WEBSERVICE_URL,WEBSERVICE_SendPostNew,[AppDelegate getAppDelegate].userCode,@"message",[_txtContent.text urlencode],_selectedNewsType];
    if (dic==nil) {
        string = [NSString stringWithFormat:@"%@%@%@/%@/%@/%@",WEBSERVICE_URL,WEBSERVICE_SendPost,[AppDelegate getAppDelegate].userCode,@"message",[_txtContent.text urlencode],_selectedNewsType];
    }
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    manager.requestSerializer = [AFJSONRequestSerializer serializer];
    
    
    [manager POST:string parameters:dic
          success:^(AFHTTPRequestOperation *operation, id responseObject)
     {
         NSLog(@"JSON: %@", responseObject);
         
         NSString *result = [responseObject objectForKey:@"resultDataType"];
         
         NSLog(@"jsonArray: %@", result);
         
         if([result isEqualToString:@"1"]){
             
             [MBProgressHUD hideHUDForView:self.view animated:YES];
             
             [Utils simpleAlert:@"发送成功" withDelegate:self];
         }else{
             [MBProgressHUD hideHUDForView:self.view animated:YES];
             [Utils simpleAlert:@"发送失败"];
         }
     }
          failure:
     ^(AFHTTPRequestOperation *operation, NSError *error) {
         NSLog(@"Error: %@", error);
     }];

}



//#pragma mark - 键盘处理
//#pragma mark 键盘即将显示
- (void)keyBoardWillShow:(NSNotification *)note{
    
    
    CGRect rect = [note.userInfo[UIKeyboardFrameEndUserInfoKey] CGRectValue];
    CGFloat ty = - rect.size.height;
    [UIView animateWithDuration:[note.userInfo[UIKeyboardAnimationDurationUserInfoKey] doubleValue] animations:^{
        
            CGRect txtrect = _toolview.frame;
            txtrect.origin.y=self.view.frame.size.height+ty-_toolview.frame.size.height;
            _toolview.frame = txtrect;
        
    }];
}
#pragma mark 键盘即将退出
- (void)keyBoardWillHide:(NSNotification *)note{
    
    [UIView animateWithDuration:[note.userInfo[UIKeyboardAnimationDurationUserInfoKey] doubleValue] animations:^{
        CGRect txtrect = _toolview.frame;
        txtrect.origin.y=self.view.frame.size.height-_toolview.frame.size.height;
        _toolview.frame = txtrect;
        
        CGRect imgrect = _previewImg.frame;
        imgrect.origin.y=_toolview.frame.origin.y-_previewImg.frame.size.height-5;
        _previewImg.frame = txtrect;
    }];
}

- (IBAction)clickPhoto:(id)sender {
    UIImagePickerController *picker = [[UIImagePickerController alloc] init];
    picker.delegate = self;
    picker.sourceType = UIImagePickerControllerSourceTypeCamera;
    picker.cameraCaptureMode = UIImagePickerControllerCameraCaptureModePhoto;
    picker.allowsEditing = YES;
    picker.showsCameraControls = YES;
    
    
    NSArray *sourceTypes =
    [UIImagePickerController availableMediaTypesForSourceType:picker.sourceType];
    if (![sourceTypes containsObject:(NSString *)kUTTypeMovie ]){
        NSLog(@"Can't save videos");
    }
    [self presentViewController:picker animated:YES completion:nil];
}

- (IBAction)clickAlbum:(id)sender {
    UIImagePickerController *picker = [[UIImagePickerController alloc] init];
    picker.delegate = self;
    picker.allowsEditing = YES;
    picker.sourceType = UIImagePickerControllerSourceTypeSavedPhotosAlbum;
    
    
    NSArray *sourceTypes =
    [UIImagePickerController availableMediaTypesForSourceType:picker.sourceType];
    if (![sourceTypes containsObject:(NSString *)kUTTypeMovie ]){
        NSLog(@"Can't save videos");
    }
    
    [self presentViewController:picker animated:YES completion:nil];
}

- (IBAction)clickAt:(id)sender {
    [self.navigationController pushViewController:_mlvc animated:YES];
}


#pragma mark video delgate



- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info
{
    NSString *mediaType = [info objectForKey:UIImagePickerControllerMediaType];
    if ([mediaType isEqualToString:(NSString *)kUTTypeImage]){
        
        if (picker.sourceType==UIImagePickerControllerSourceTypeCamera) {
            [self pickVideoWithInfo:info type:1];
        }else{
            [self pickVideoWithInfo:info type:0];
        }
        
        
    }
    [picker dismissViewControllerAnimated:YES completion:nil];
}



- (void)pickVideoWithInfo:(NSDictionary *)info type:(int)type
{
    
    
    UIImage *img = [info objectForKey:UIImagePickerControllerEditedImage];
    UIImage *sThumbnail = [img scaleToSize:CGSizeMake(500, 500)];
    _previewImg.image = img;

    
    //NSData* imageData = UIImagePNGRepresentation(img);
    NSData* smallData = UIImageJPEGRepresentation(sThumbnail, 1);
    _savepath = [NSTemporaryDirectory() stringByAppendingString:@"/userdefine.jpg"];
    [smallData writeToFile:_savepath atomically:YES];
    NSLog(@"data:%@",smallData);
    
    if (type==1) {
        UIImageWriteToSavedPhotosAlbum (img, nil, nil, nil);
    }
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark Picker Data Source Methods

- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView {
    
    return 1;
}

- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component {
    
    return [_newsTypeArray count];
}

#pragma mark Picker Delegate Methods
- (NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component {
    
    return [[_newsTypeArray objectAtIndex:row] objectForKey:@"osccName"];
}

- (void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component{
    
    self.newsTypeTxtFild.text = [[_newsTypeArray objectAtIndex:row] objectForKey:@"osccName"];
    _selectedNewsType = [[_newsTypeArray objectAtIndex:row] objectForKey:@"osccCode"];
}

@end
