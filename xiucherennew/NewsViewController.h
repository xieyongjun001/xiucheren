//
//  NewsViewController.h
//  xiucherennew
//
//  Created by harry on 13-11-14.
//  Copyright (c) 2013年 harry. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MessageView.h"

@interface NewsViewController : UIViewController<MessageDelegate>

@property (weak, nonatomic) IBOutlet UILabel *lblTitle;

- (IBAction)clickTosend:(id)sender;
- (IBAction)clickCheckin:(id)sender;
@end
