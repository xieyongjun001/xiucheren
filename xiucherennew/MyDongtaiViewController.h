//
//  MyDongtaiViewController.h
//  xiucherennew
//
//  Created by harry on 13-11-10.
//  Copyright (c) 2013年 harry. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MessageView.h"

@interface MyDongtaiViewController : UIViewController<UIActionSheetDelegate,UIImagePickerControllerDelegate,UINavigationControllerDelegate,MessageDelegate>

@property (weak, nonatomic) IBOutlet UIButton *btHeader;
@property (weak, nonatomic) IBOutlet UILabel *lblName;
@property (weak, nonatomic) IBOutlet UILabel *lblCompInfo;
- (IBAction)clickSend:(id)sender;
- (IBAction)clickContact:(id)sender;
- (IBAction)clickMyInfo:(id)sender;
- (IBAction)clickHeader:(id)sender;
@end
