//
//  Reg1ViewController.h
//  xiucherennew
//
//  Created by harry on 13-11-9.
//  Copyright (c) 2013年 harry. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface Reg1ViewController : UIViewController<UIAlertViewDelegate>

@property (weak, nonatomic) IBOutlet UITextField *txtRecman;

- (IBAction)clickBack:(id)sender;
- (IBAction)clickNext:(id)sender;
@end
