//
//  MoreHomeViewController.m
//  xiucherennew
//
//  Created by harry on 13-11-11.
//  Copyright (c) 2013年 harry. All rights reserved.
//

#import "MoreHomeViewController.h"
#import "MoreMemberListViewController.h"
#import "HomeViewController.h"


@interface MoreHomeViewController ()

@end

@implementation MoreHomeViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
	// Do any additional setup after loading the view.
}

- (void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    [self.navigationController setNavigationBarHidden:YES];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)clickMembers:(id)sender {
    
    MoreMemberListViewController *mlvc = [[MoreMemberListViewController alloc] initWithNibName:@"MoreMemberListViewController" bundle:nil];
    mlvc.gosixin = 1;
    [self.navigationController pushViewController:mlvc animated:YES];

}

- (IBAction)clickSixin:(id)sender {
    
    MoreMemberListViewController *mlvc = [[MoreMemberListViewController alloc] initWithNibName:@"MoreMemberListViewController" bundle:nil];
    mlvc.gosixin = 2;
    [self.navigationController pushViewController:mlvc animated:YES];
    
}

- (IBAction)clickLogout:(id)sender {
    [AppDelegate getAppDelegate].userCode = @"";
    [AppDelegate getAppDelegate].userDict = nil;
    HomeViewController *home = [[HomeViewController alloc] initWithNibName:@"HomeViewController" bundle:nil];
    UINavigationController *navigation=[[UINavigationController alloc] initWithRootViewController:home];
    navigation.navigationBarHidden=YES;
    [self presentViewController:navigation animated:YES completion:nil];
}
@end
