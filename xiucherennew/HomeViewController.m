//
//  HomeViewController.m
//  xiucherennew
//
//  Created by harry on 13-11-9.
//  Copyright (c) 2013年 harry. All rights reserved.
//

#import "HomeViewController.h"
#import "Reg1ViewController.h"
#import "ForgetPass1ViewController.h"

@interface HomeViewController ()
@property(nonatomic,strong)UIImageView *imgv;
@end

@implementation HomeViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    _imgv = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"loading"]];
    _imgv.frame = CGRectMake(0, 0, 320, 568);
    [self.view addSubview:_imgv];
    
    [NSTimer scheduledTimerWithTimeInterval:3.0 target:self selector:@selector(dissmiss) userInfo:nil repeats:YES];
    
}

- (void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    
    
}

- (void)dissmiss{
    [_imgv removeFromSuperview];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event{
    [_txtAccount resignFirstResponder];
    [_txtPass resignFirstResponder];

}

- (IBAction)clickLogin:(id)sender {
    /*
    [self dismissViewControllerAnimated:YES completion:nil];
    [AppDelegate getAppDelegate].userCode = @"admin";
    */
    if ([_txtAccount.text length]==0 || [_txtPass.text length]==0) {
        [Utils simpleAlert:@"请填写用户名密码"];
        return ;
    }
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    NSString *string = [NSString stringWithFormat:@"%@%@%@/%@",WEBSERVICE_URL,WEBSERVICE_Login,_txtAccount.text,_txtPass.text];
    if (![[AppDelegate getAppDelegate].bduid isEqualToString:@""]) {
        string = [string stringByAppendingFormat:@"/%@/%@",[AppDelegate getAppDelegate].bdcid,[AppDelegate getAppDelegate].bduid];
    }
    [manager GET:string parameters:nil success:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        NSLog(@"JSON: %@", responseObject);
      
        NSString *result = [responseObject objectForKey:@"resultDataType"];

        NSLog(@"jsonArray: %@", result);
        
        if([result isEqualToString:@"1"]){
            NSArray *resultlist = [responseObject objectForKey:@"resultList"];
            
            NSLog(@"resultlist:%@",resultlist);
            NSLog(@"auserName:%@",[[resultlist objectAtIndex:0] objectForKey:@"auserName"]);
            
            [AppDelegate getAppDelegate].userCode = [[resultlist objectAtIndex:0] objectForKey:@"auserCode"];
            [AppDelegate getAppDelegate].userDict = [resultlist objectAtIndex:0];
            
            NSString *msg =@"登录成功。";
            if ([[[resultlist objectAtIndex:0] objectForKey:@"auserLoginState"] intValue]==1) {
                if ([[[resultlist objectAtIndex:0] objectForKey:@"auserLoginLog"] intValue]==1) {
                    msg = [msg stringByAppendingString:@"您的帐户在手机上正在登陆中，请注意帐户安全。"];
                }else{
                    msg = [msg stringByAppendingString:@"您的帐户在电脑上正在登陆中，请注意帐户安全。"];
                }
            }
            [Utils simpleAlert:msg withDelegate:self];
            [MBProgressHUD hideHUDForView:self.view animated:YES];
        }else{
            [Utils simpleAlert:@"用户名密码不正确"];
            [MBProgressHUD hideHUDForView:self.view animated:YES];
        }
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"Error: %@", error);
    }];

}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (IBAction)clickReg:(id)sender {
    Reg1ViewController *rvc = [[Reg1ViewController alloc] initWithNibName:@"Reg1ViewController" bundle:nil];
    [self.navigationController pushViewController:rvc animated:YES];
}

- (IBAction)clickForgetpass:(id)sender {
    ForgetPass1ViewController *rvc = [[ForgetPass1ViewController alloc] initWithNibName:@"ForgetPass1ViewController" bundle:nil];
    [self.navigationController pushViewController:rvc animated:YES];
}
@end
