//
//  MoreFeedViewController.h
//  xiucherennew
//
//  Created by harry on 13-11-12.
//  Copyright (c) 2013年 harry. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MoreFeedViewController : UIViewController

@property (weak, nonatomic) IBOutlet GCPlaceholderTextView *txtContent;
- (IBAction)clickCancel:(id)sender;
- (IBAction)clickSend:(id)sender;
@end
