//
//  CommentView.h
//  xiucherennew
//
//  Created by harry on 14-2-19.
//  Copyright (c) 2014年 harry. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CommentView : UIView
- (id)initWithFrame:(CGRect)frame withData:(NSDictionary *)data;

@end
