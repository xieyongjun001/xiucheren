//
//  MessageView.h
//  xiucherennew
//
//  Created by harry on 13-11-13.
//  Copyright (c) 2013年 harry. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Post.h"

@protocol MessageDelegate<NSObject>

- (void)didClickRight:(NSString *)index;
- (void)didClickLeft:(Post *)postData;
- (void)didClickCenter:(Post *)postData;
//- (void)didClickLike:(Post *)postData;
@end


@interface MessageView : UIView
@property (nonatomic,retain) id <MessageDelegate> delegate;
@property (nonatomic,assign) int type;  //默认是0 列表里面显示；1表示正文显示
@property (nonatomic,retain) Post *post;
@property (nonatomic,retain) UIButton *likebt;
@property (nonatomic,retain) UIButton *lkbt;
@property (nonatomic,retain) UILabel *likeLabel;
- (id)initWithFrame:(CGRect)frame withData:(Post *)data;
- (id)initWithFrame:(CGRect)frame withData:(Post *)data type:(int)type;

@end
