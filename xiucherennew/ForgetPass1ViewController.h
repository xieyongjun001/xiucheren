//
//  ForgetPass1ViewController.h
//  xiucherennew
//
//  Created by harry on 13-12-8.
//  Copyright (c) 2013年 harry. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ForgetPass1ViewController : UIViewController<UIAlertViewDelegate>
@property (weak, nonatomic) IBOutlet UITextField *txtAccount;
@property (weak, nonatomic) IBOutlet UITextField *txtRecommend;
@property (weak, nonatomic) IBOutlet UITextField *txtName;
@property (weak, nonatomic) IBOutlet UITextField *txtMobile;
@property (weak, nonatomic) IBOutlet UITextField *txtEmail;

- (IBAction)clickBack:(id)sender;
- (IBAction)clickSubmit:(id)sender;
@end
