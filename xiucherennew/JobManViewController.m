//
//  JobManViewController.m
//  xiucherennew
//
//  Created by harry on 13-11-16.
//  Copyright (c) 2013年 harry. All rights reserved.
//

#import "JobManViewController.h"

@interface JobManViewController ()
@property (strong, nonatomic)NSArray *mblist;
@property (strong, nonatomic)NSMutableArray *selectedMb;
@end

@implementation JobManViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    //_mblist = [NSArray arrayWithObjects:@"大毛", @"二毛", @"三毛", nil];
    
    _selectedMb = [[NSMutableArray alloc] init];
    
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    NSDictionary *dic = [AppDelegate getAppDelegate].userDict;
    NSString *string = [NSString stringWithFormat:@"%@%@%@",WEBSERVICE_URL,WEBSERVICE_GetStaff,[dic objectForKey:@"auorgCode"]];
    //NSString *string = [NSString stringWithFormat:@"%@%@%@",WEBSERVICE_URL,WEBSERVICE_GetStaff,@"ROOT"];
    
    [manager GET:string parameters:nil success:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        NSLog(@"JSON: %@", responseObject);
        
        NSString *result = [responseObject objectForKey:@"resultDataType"];
        
        NSLog(@"jsonArray: %@", result);
        
        if([result isEqualToString:@"1"]){
            NSArray *arr = [responseObject objectForKey:@"resultList"];
            if([arr count]>0){
                NSDictionary *dic = [arr objectAtIndex:0];
                _mblist = [dic objectForKey:@"oaUserInfos"];
                [_tbview reloadData];
            }
        }
        
        
        [MBProgressHUD hideHUDForView:self.view animated:YES];
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"Error: %@", error);
        [MBProgressHUD hideHUDForView:self.view animated:YES];
    }];
    
    [_tbview setAllowsSelection:NO];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}




#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    // Return the number of rows in the section.
    return [_mblist count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
    }

    NSDictionary *dic= [_mblist objectAtIndex:indexPath.row];
    
    
    UIImage *img=[UIImage imageNamed:@"assignjob_unselected"];
    UIButton *bt = [[UIButton alloc] initWithFrame:CGRectMake(15,19,img.size.width/2,img.size.height/2)];
    [bt setImage:img forState:UIControlStateNormal];
    bt.tag = indexPath.row+1;
    [bt addTarget:self action:@selector(statusClick:) forControlEvents:UIControlEventTouchUpInside];
    [cell.contentView addSubview:bt];
    
    UILabel *lblTitle = [[UILabel alloc] initWithFrame:CGRectMake(70, 21,200,16)];
    lblTitle.font = [UIFont boldSystemFontOfSize:16];
    lblTitle.textColor = [UIColor blackColor];
    lblTitle.backgroundColor = [UIColor clearColor];
    lblTitle.opaque = NO;
    lblTitle.tag = 2000+indexPath.section*100+indexPath.row;
    lblTitle.textAlignment = NSTextAlignmentLeft;
    lblTitle.text = [dic objectForKey:@"auserName"];
    [cell.contentView addSubview:lblTitle];
    
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 60;
}



/*
 // Override to support conditional editing of the table view.
 - (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
 {
 // Return NO if you do not want the specified item to be editable.
 return YES;
 }
 */

/*
 // Override to support editing the table view.
 - (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
 {
 if (editingStyle == UITableViewCellEditingStyleDelete) {
 // Delete the row from the data source
 [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
 }
 else if (editingStyle == UITableViewCellEditingStyleInsert) {
 // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
 }
 }
 */

/*
 // Override to support rearranging the table view.
 - (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath
 {
 }
 */

/*
 // Override to support conditional rearranging of the table view.
 - (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath
 {
 // Return NO if you do not want the item to be re-orderable.
 return YES;
 }
 */


#pragma mark - Table view delegate

// In a xib-based application, navigation from a table can be handled in -tableView:didSelectRowAtIndexPath:


- (void)statusClick:(UIButton *)bt{
    int row = bt.tag-1;
    
    
    NSDictionary *dic = [_mblist objectAtIndex:row];
    NSString *mbid = [dic objectForKey:@"auserCode"];
    UIImage *img=[UIImage imageNamed:@"assignjob_selected"];
    if ([_selectedMb indexOfObject:mbid]==NSNotFound) {
        [_selectedMb addObject:mbid];
    }else{
        img=[UIImage imageNamed:@"assignjob_unselected"];
        [_selectedMb removeObject:mbid];
    }
    
    [bt setImage:img forState:UIControlStateNormal];
    bt.frame = CGRectMake(15,19,img.size.width/2,img.size.height/2);

}

- (IBAction)clickBack:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)clickSend:(id)sender {
    //@Path("/updateTask/{userCode}/{jsonString}")
    //NSDictionary *dict = @{@"rowGuid": _txtTitle.text,@"taskPerson": [AppDelegate getAppDelegate].userCode,@"content": _txtContent.text};
    //NSString *output = [dict JSONRepresentation];
    
    __block int uploadcnt = 0;
    for (int i=0; i<[_jobs count]; i++) {
        NSDictionary *dicall = [_jobs objectAtIndex:i];
        NSString *contents = [dicall objectForKey:@"content"];
        NSString *publishPerson = [dicall objectForKey:@"publishPerson"];
        NSString *taskName = [dicall objectForKey:@"taskName"];
        NSString *taskGuid = [dicall objectForKey:@"taskGuid"];
        
        NSDictionary *dict = @{@"taskGuid": taskGuid,@"taskName": taskName,@"publishPerson": publishPerson,@"content": contents};
        NSString *output = [dict JSONRepresentation];
        
        //NSDictionary *dict2 = @{@"taskPerson":_selectedMb};
        NSMutableArray *arr5 = [[NSMutableArray alloc] init];
        for (int i=0; i<[_selectedMb count]; i++) {
            [arr5 addObject:@{@"taskPerson":[_selectedMb objectAtIndex:i]}];
        }
        NSString *output2 = [arr5 JSONRepresentation];
        
        [MBProgressHUD showHUDAddedTo:self.view animated:YES];
        AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
        NSString *string = [NSString stringWithFormat:@"%@%@%@/%@/%@",WEBSERVICE_URL,WEBSERVICE_UpdateTaskInfoo,[AppDelegate getAppDelegate].userCode,[output urlencode],[output2 urlencode]];
        
        [manager POST:string parameters:nil success:^(AFHTTPRequestOperation *operation, id responseObject) {
            
            NSLog(@"JSON: %@", responseObject);
            
            NSString *result = [responseObject objectForKey:@"resultDataType"];
            
            NSLog(@"jsonArray: %@", result);
            
            if([result isEqualToString:@"1"]){
                [Utils simpleAlert:[NSString stringWithFormat:@"%@提交成功",taskName]];
                uploadcnt++;
                if (uploadcnt==[_jobs count]) {
                    [self.navigationController popViewControllerAnimated:YES];
                }
            }
            
            
            [MBProgressHUD hideHUDForView:self.view animated:YES];
        } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
            NSLog(@"Error: %@", error);
            [MBProgressHUD hideHUDForView:self.view animated:YES];
        }];
    }
    
    
}
@end
