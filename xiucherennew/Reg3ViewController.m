//
//  Reg3ViewController.m
//  xiucherennew
//
//  Created by harry on 13-11-9.
//  Copyright (c) 2013年 harry. All rights reserved.
//

#import "Reg3ViewController.h"
#import "Reg4ViewController.h"

@interface Reg3ViewController ()
@property(nonatomic,strong)NSArray *citys;
@property(nonatomic,strong)NSArray *orgs;
@property(nonatomic,strong)NSArray *brands;
@property(nonatomic,strong)NSArray *comptype;

@property(nonatomic,strong)UIPickerView *companyTypePicker;
@property(nonatomic,strong)UIPickerView *areaPicker;
@property(nonatomic,strong)UIPickerView *provincePicker;
@property(nonatomic,strong)UIPickerView *cityTypePicker;
@property(nonatomic,strong)UIPickerView *brandTypePicker;
@property(nonatomic,strong)UIPickerView *companyPicker;

@property(nonatomic,strong)NSString *curarea;
@property(nonatomic,strong)NSString *curprovince;

@property(nonatomic,strong)NSString *brandCode;
@property(nonatomic,strong)NSString *comptypeCode;
@property(nonatomic,strong)NSString *areaCode;
@property(nonatomic,strong)NSString *auorgGuid;
@end

@implementation Reg3ViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

#pragma mark - Touches
-(void)viewTapped:(UITapGestureRecognizer*)tapGr
{
    [_txtCompanyType resignFirstResponder];
    [_txtArea resignFirstResponder];
    [_txtProvince resignFirstResponder];
    [_txtCity resignFirstResponder];
    [_txtBrand resignFirstResponder];
    [_txtCompany resignFirstResponder];
    
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    _citys = [[NSArray alloc] init];
    _comptype = [[NSArray alloc] init];
    _brands = [[NSArray alloc] init];
    _orgs = [[NSArray alloc] init];
    _curarea = @"";
    _curprovince = @"";
    
    _brandCode = @"";
    _comptypeCode = @"";
    _areaCode = @"";
    
    UITapGestureRecognizer *tapGr = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(viewTapped:)];
    tapGr.cancelsTouchesInView = NO;
    [_scl addGestureRecognizer:tapGr];
    
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    NSString *string = [NSString stringWithFormat:@"%@%@",WEBSERVICE_URL,WEBSERVICE_GetCity];
    
    [manager GET:string parameters:nil success:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        NSLog(@"JSON: %@", responseObject);
        
        NSString *result = [responseObject objectForKey:@"resultDataType"];
        
        NSLog(@"jsonArray: %@", result);
        
        if([result isEqualToString:@"1"]){
            _citys = [responseObject objectForKey:@"resultList"];
        }
        
        [MBProgressHUD hideHUDForView:self.view animated:YES];
        
        float y = 0;
       
        
        _areaPicker = [[UIPickerView alloc] initWithFrame:CGRectMake(0, y, 320, 216)];
        _areaPicker.delegate = self;
        _areaPicker.dataSource = self;
        _areaPicker.showsSelectionIndicator = YES;
        _areaPicker.tag = 102;
        _txtArea.inputView =_areaPicker;
        
        _provincePicker = [[UIPickerView alloc] initWithFrame:CGRectMake(0, y, 320, 216)];
        _provincePicker.delegate = self;
        _provincePicker.dataSource = self;
        _provincePicker.showsSelectionIndicator = YES;
        _provincePicker.tag = 103;
        _txtProvince.inputView =_provincePicker;
        
        _cityTypePicker = [[UIPickerView alloc] initWithFrame:CGRectMake(0, y, 320, 216)];
        _cityTypePicker.delegate = self;
        _cityTypePicker.dataSource = self;
        _cityTypePicker.showsSelectionIndicator = YES;
        _cityTypePicker.tag = 104;
        _txtCity.inputView =_cityTypePicker;
     
        
        
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"Error: %@", error);
        [MBProgressHUD hideHUDForView:self.view animated:YES];
    }];
    
    
    _companyPicker = [[UIPickerView alloc] initWithFrame:CGRectMake(0, 0, 320, 216)];
    _companyPicker.delegate = self;
    _companyPicker.dataSource = self;
    _companyPicker.showsSelectionIndicator = YES;
    _companyPicker.tag = 106;
    _txtCompany.inputView =_companyPicker;
    
  
    
    AFHTTPRequestOperationManager *manager3 = [AFHTTPRequestOperationManager manager];
    NSString *string3 = [NSString stringWithFormat:@"%@%@",WEBSERVICE_URL,WEBSERVICE_GetOrgBrand];
    
    [manager3 GET:string3 parameters:nil success:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        NSLog(@"JSON: %@", responseObject);
        
        NSString *result = [responseObject objectForKey:@"resultDataType"];
        
        NSLog(@"jsonArray: %@", result);
        
        if([result isEqualToString:@"1"]){
            _brands = [responseObject objectForKey:@"resultList"];
        }
        
        
        _brandTypePicker = [[UIPickerView alloc] initWithFrame:CGRectMake(0, 0, 320, 216)];
        _brandTypePicker.delegate = self;
        _brandTypePicker.dataSource = self;
        _brandTypePicker.showsSelectionIndicator = YES;
        _brandTypePicker.tag = 105;
        _txtBrand.inputView =_brandTypePicker;
        
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"Error: %@", error);
    }];
    
    AFHTTPRequestOperationManager *manager4 = [AFHTTPRequestOperationManager manager];
    NSString *string4 = [NSString stringWithFormat:@"%@%@",WEBSERVICE_URL,WEBSERVICE_GetOrgType];
    
    [manager4 GET:string4 parameters:nil success:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        NSLog(@"JSON: %@", responseObject);
        
        NSString *result = [responseObject objectForKey:@"resultDataType"];
        
        NSLog(@"jsonArray: %@", result);
        
        if([result isEqualToString:@"1"]){
            _comptype = [responseObject objectForKey:@"resultList"];
        }
        
        
        _companyTypePicker = [[UIPickerView alloc] initWithFrame:CGRectMake(0, 0, 320, 216)];
        _companyTypePicker.delegate = self;
        _companyTypePicker.dataSource = self;
        _companyTypePicker.showsSelectionIndicator = YES;
        _companyTypePicker.tag = 101;
        _txtCompanyType.inputView =_companyTypePicker;
        
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"Error: %@", error);
    }];

    
   
}


- (void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    _scl.contentSize = CGSizeMake(320, 625);
}

- (void)touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event{
    [_txtCompanyType resignFirstResponder];
    [_txtArea resignFirstResponder];
    [_txtProvince resignFirstResponder];
    [_txtCity resignFirstResponder];
    [_txtBrand resignFirstResponder];
    [_txtCompany resignFirstResponder];
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)clickNext:(id)sender {
    if ([_areaCode length]>0) {
        AFHTTPRequestOperationManager *manager2 = [AFHTTPRequestOperationManager manager];
        NSString *string2 = [NSString stringWithFormat:@"%@%@",_querystring,_auorgGuid];
        
        [manager2 POST:string2 parameters:nil success:^(AFHTTPRequestOperation *operation, id responseObject) {
            
            NSLog(@"JSON: %@", responseObject);
            
            NSString *result = [responseObject objectForKey:@"resultDataType"];
            
            NSLog(@"jsonArray: %@", result);
            
            if([result isEqualToString:@"1"]){
                Reg4ViewController *rvc = [[Reg4ViewController alloc] initWithNibName:@"Reg4ViewController" bundle:nil];
                [self.navigationController pushViewController:rvc animated:YES];
            }
            
        } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
            NSString *err = [NSString stringWithFormat:@"注册失败 %@",error];
            [Utils simpleAlert:err];
        }];
    }else{
        [Utils simpleAlert:@"请填写完整信息"];
    }
}




- (IBAction)clickBack:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}


- (NSArray *)getChildren:(NSString *)parentid{
    NSMutableArray *t = [[NSMutableArray alloc] init];
    for (int i=0; i<[_citys count]; i++) {
        NSDictionary *dic = [_citys objectAtIndex:i];
        if ([[dic objectForKey:@"auarParentGuid"] isEqualToString:parentid]) {
            [t addObject:dic];
        }
    }
    return t;
}


- (void)loadCompArr{
    //@Path("/getOrgInfo/{orgTypeCode}/{brandCode}/{areaGuid}")
    /**
	 * 获取公司信息
	 * @param orgTypeCode 公司类型
	 * @param brandCode 品牌
	 * @param areaGuid 区域（城市信息的主键）
	 * @return List<OaOrgInfo>
	 */

    
    if ([_brandCode length]>0 && [_areaCode length]>0 && [_comptypeCode length]>0) {
        AFHTTPRequestOperationManager *manager2 = [AFHTTPRequestOperationManager manager];
        NSString *string2 = [NSString stringWithFormat:@"%@%@%@/%@/%@",WEBSERVICE_URL,WEBSERVICE_GetOrg,_comptypeCode,_brandCode,_areaCode];
        
        [manager2 GET:string2 parameters:nil success:^(AFHTTPRequestOperation *operation, id responseObject) {
            
            NSLog(@"JSON: %@", responseObject);
            
            NSString *result = [responseObject objectForKey:@"resultDataType"];
            
            NSLog(@"jsonArray: %@", result);
            
            if([result isEqualToString:@"1"]){
                _orgs = [responseObject objectForKey:@"resultList"];
                [_companyPicker reloadAllComponents];
            }
            
        } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
            NSLog(@"Error: %@", error);
        }];
    }
    
}


#pragma mark - picker
- (void)pickerView:(UIPickerView *)pickerView didSelectRow: (NSInteger)row inComponent:(NSInteger)component {
    // Handle the selection
    if (row==0) {
        switch (pickerView.tag) {
            case 101:{
                _txtCompanyType.text = @"";
                _comptypeCode = @"";
                [self loadCompArr];
                break;
            }
            case 102:{
                _txtArea.text =  @"";
                _curarea =  @"";
                [_provincePicker reloadAllComponents];
                break;
            }
            case 103:{
                if(![_curarea isEqualToString:@""]){
                    _txtProvince.text =  @"";
                    _curprovince =  @"";
                    [_cityTypePicker reloadAllComponents];
                }
                break;
            }
            case 104:{
                if(![_curprovince isEqualToString:@""]){
                    _txtCity.text =  @"";
                    _areaCode =  @"";
                    [self loadCompArr];
                }
                break;
            }
            case 105:{
                _txtBrand.text =  @"";
                _brandCode =  @"";
                [self loadCompArr];
                break;
            }
            case 106:{
                _txtCompany.text =  @"";
                _lblCompanyAddress.text =  @"";
                _lblCompanyCode.text =  @"";
                _auorgGuid = @"";
                break;
            }
                
            default:
                break;
        }
    }else{
        row--;
        switch (pickerView.tag) {
            case 101:{
                NSDictionary *dic = [_comptype objectAtIndex:row];
                _txtCompanyType.text = [dic objectForKey:@"osccName"];
                _comptypeCode = [dic objectForKey:@"osccCode"];
                [self loadCompArr];
                break;
            }
            case 102:{
                NSArray *myarea = [self getChildren:@"1"];
                NSDictionary *dic = [myarea objectAtIndex:row];
                _txtArea.text = [dic objectForKey:@"auarName"];
                _curarea = [dic objectForKey:@"auarGuid"];
                [_provincePicker reloadAllComponents];
                break;
            }
            case 103:{
                if(![_curarea isEqualToString:@""]){
                    NSArray *myarea = [self getChildren:_curarea];
                    NSDictionary *dic = [myarea objectAtIndex:row];
                    _txtProvince.text = [dic objectForKey:@"auarName"];
                    _curprovince = [dic objectForKey:@"auarGuid"];
                    [_cityTypePicker reloadAllComponents];
                }
                break;
            }
            case 104:{
                if(![_curprovince isEqualToString:@""]){
                    NSArray *myarea = [self getChildren:_curprovince];
                    NSDictionary *dic = [myarea objectAtIndex:row];
                    _txtCity.text = [dic objectForKey:@"auarName"];
                    _areaCode = [dic objectForKey:@"auarGuid"];
                    [self loadCompArr];
                }
                break;
            }
            case 105:{
                NSDictionary *dic = [_brands objectAtIndex:row];
                _txtBrand.text = [dic objectForKey:@"osccName"];
                _brandCode = [dic objectForKey:@"osccCode"];
                [self loadCompArr];
                break;
            }
            case 106:{
                NSDictionary *dic = [_orgs objectAtIndex:row];
                _txtCompany.text = [dic objectForKey:@"auorgName"];
                _lblCompanyAddress.text = [dic objectForKey:@"auorgAddress"];
                _lblCompanyCode.text = [dic objectForKey:@"auorgCode"];
                _auorgGuid =[dic objectForKey:@"auorgGuid"];
                break;
            }
                
            default:
                break;
        }
    }
    
}

// tell the picker how many rows are available for a given component
- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component {
    switch (pickerView.tag) {
        case 101:
            return [_comptype count]+1;
            break;
        case 102:{
            NSArray *myarea = [self getChildren:@"1"];
            return [myarea count]+1;
        }
            
        case 103:{
            if(![_curarea isEqualToString:@""]){
                NSArray *myarea = [self getChildren:_curarea];
               return [myarea count]+1;
            }
            break;
        }
        case 104:{
            if(![_curprovince isEqualToString:@""]){
                NSArray *myarea = [self getChildren:_curprovince];
                return [myarea count]+1;
            }
            break;
        }
        case 105:
            return [_brands count]+1;
            break;
        case 106:
            return [_orgs count]+1;
            break;
            
        default:
            break;
    }
    
    return 0;
}

// tell the picker how many components it will have
- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView {
    return 1;
}

// tell the picker the title for a given component
- (NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component {
    
    if (row==0) {
        return @"请选择";
    }else{
        row--;
        
        switch (pickerView.tag) {
            case 101:{
                NSDictionary *dic = [_comptype objectAtIndex:row];
                return [dic objectForKey:@"osccName"];
                break;
            }
            case 102:{
                NSArray *myarea = [self getChildren:@"1"];
                NSDictionary *dic = [myarea objectAtIndex:row];
                return [dic objectForKey:@"auarName"];
                break;
            }
            case 103:{
                if(![_curarea isEqualToString:@""]){
                    NSArray *myarea = [self getChildren:_curarea];
                    NSDictionary *dic = [myarea objectAtIndex:row];
                    return [dic objectForKey:@"auarName"];
                }
                break;
            }
            case 104:{
                if(![_curprovince isEqualToString:@""]){
                    NSArray *myarea = [self getChildren:_curprovince];
                    NSDictionary *dic = [myarea objectAtIndex:row];
                    return [dic objectForKey:@"auarName"];
                }
                break;
            }
            case 105:{
                NSDictionary *dic = [_brands objectAtIndex:row];
                return [dic objectForKey:@"osccName"];
                break;
            }
                
            case 106:{
                NSDictionary *dic = [_orgs objectAtIndex:row];
                return [dic objectForKey:@"auorgName"];
                break;
            }
            default:
                break;
        }
    }
    
    return @"";
}

// tell the picker the width of each row for a given component
- (CGFloat)pickerView:(UIPickerView *)pickerView widthForComponent:(NSInteger)component {
    int sectionWidth = 300;
    
    return sectionWidth;
}

@end
