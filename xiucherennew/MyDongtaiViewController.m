//
//  MyDongtaiViewController.m
//  xiucherennew
//
//  Created by harry on 13-11-10.
//  Copyright (c) 2013年 harry. All rights reserved.
//

#import "MyDongtaiViewController.h"
#import "Post.h"
#import "SendNewsViewController.h"
#import "MoreMemberListViewController.h"
#import "Reg2ViewController.h"
#import "EditUserViewController.h"
#import "UIImage+Thumb.h"
#import "CommentListViewController.h"
#import "CommentSendViewController.h"
#import "ResentViewController.h"
#import "NSData+base64DataFromString.h"

@interface MyDongtaiViewController ()
@property (strong, nonatomic) UIScrollView *sclview;
@property (strong, nonatomic) NSMutableArray *postlist;
@property (assign, nonatomic) int picktype;
@end

@implementation MyDongtaiViewController
@synthesize sclview,picktype;
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self.navigationController setNavigationBarHidden:YES];
	// Do any additional setup after loading the view.
    
    NSDictionary *ud = [AppDelegate getAppDelegate].userDict;
    _lblName.text = [ud objectForKey:@"auserName"];
    _lblCompInfo.text = [NSString stringWithFormat:@"%@ %@",[ud objectForKey:@"auorgName"],[ud objectForKey:@"auorgCode"]];
    sclview = [[UIScrollView alloc] initWithFrame:CGRectMake(0, 143, 320, 450)];
    sclview.backgroundColor = [UIColor clearColor];
    sclview.bounces = YES;
    [self.view addSubview:sclview];
    
    NSDictionary *userdic = [AppDelegate getAppDelegate].userDict;
    [_btHeader setImageWithURL:[NSURL URLWithString:[userdic objectForKey:@"userHeadImagePath"]] forState:UIControlStateNormal];
    
}

- (void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    
    
    
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    NSString *string = [NSString stringWithFormat:@"%@%@%@/myPage",WEBSERVICE_URL,WEBSERVICE_GetPost,[AppDelegate getAppDelegate].userCode];
    
    [manager GET:string parameters:nil success:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        NSLog(@"JSON: %@", responseObject);
        
        NSString *result = [responseObject objectForKey:@"resultDataType"];
        
        NSLog(@"jsonArray: %@", result);
        
        if([result isEqualToString:@"1"]){
            _postlist = [responseObject objectForKey:@"resultList"];
        }
        [self reloadViews];
        
        [MBProgressHUD hideHUDForView:self.view animated:YES];
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"Error: %@", error);
        [MBProgressHUD hideHUDForView:self.view animated:YES];
    }];
}

- (void)reloadViews{
    UIView *v;
	while ((v = [[sclview subviews] lastObject])) {
		[v removeFromSuperview];
	}
    
    float y=0;
    for (int i=0; i<[_postlist count]; i++) {
        NSDictionary *dic = [_postlist objectAtIndex:i];
        
        Post *post = [[Post alloc] init];
        post.content = [dic objectForKey:@"wordContent"];
        post.nickname = [dic objectForKey:@"postCreaterName"];;
        post.date = [dic objectForKey:@"createTime"];
        post.headerpath = [dic objectForKey:@"userImg"];
        post.postid = [dic objectForKey:@"postGuid"];
        post.zhfnum = [dic objectForKey:@"postZhuanfaNum"];
        post.huifunum = [dic objectForKey:@"postHuifuNum"];
        post.zannum = [dic objectForKey:@"postZanNum"];
        post.imageContent = [dic objectForKey:@"imageContent"];
        post.zanstatus = [[dic objectForKey:@"postInfoUserZanStatus"] intValue];
        
        NSDictionary *attributesDictionary = [NSDictionary dictionaryWithObjectsAndKeys:
                                              [UIFont systemFontOfSize:14], NSFontAttributeName,
                                              nil];
        
        
        CGFloat height = 0;
        if (![post.content isEqual: [NSNull null]] && post.content!=nil && [post.content length]>0) {
            NSMutableAttributedString *string = [[NSMutableAttributedString alloc] initWithString:post.content attributes:attributesDictionary];
            CGRect paragraphRect =
            [string boundingRectWithSize:CGSizeMake(300.f, CGFLOAT_MAX)
                                 options:(NSStringDrawingUsesLineFragmentOrigin|NSStringDrawingUsesFontLeading)
                                 context:nil];
            height = paragraphRect.size.height + 44;
        }
        
        float h = MSGBOX_BOTTOMHEIGH+MSGBOX_TOPHEIGH+height;
        if (![post.imageContent isEqualToString:@""]) {
            h+= MSGBOX_IMGHEIGH;
        }
        
        
        MessageView *mv = [[MessageView alloc] initWithFrame:CGRectMake(7, y, 307, h) withData:post];
        
        if ([post.imageContent isEqualToString:@""]) {
            y+=MSGBOX_BOTTOMHEIGH+MSGBOX_TOPHEIGH+height+MSGBOX_PADDING;
        }else{
            y+=MSGBOX_BOTTOMHEIGH+MSGBOX_TOPHEIGH+height+MSGBOX_IMGHEIGH+MSGBOX_PADDING;
        }
        [sclview addSubview:mv];
    }
    sclview.contentSize = CGSizeMake(320, y+40);
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)clickSend:(id)sender {
    SendNewsViewController *svc = [[SendNewsViewController alloc] initWithNibName:@"SendNewsViewController" bundle:nil];
    [self.navigationController pushViewController:svc animated:YES];
}

- (IBAction)clickContact:(id)sender {
    MoreMemberListViewController *svc = [[MoreMemberListViewController alloc] initWithNibName:@"MoreMemberListViewController" bundle:nil];
    svc.gosixin = 1;
    [self.navigationController pushViewController:svc animated:YES];
}

- (IBAction)clickMyInfo:(id)sender {
    EditUserViewController *rvc = [[EditUserViewController alloc] initWithNibName:@"EditUserViewController" bundle:nil];
    [self.navigationController pushViewController:rvc animated:YES];
}

- (IBAction)clickHeader:(id)sender {
    
    UIActionSheet *actionSheet = [[UIActionSheet alloc]
                                  initWithTitle:@""
                                  delegate:self
                                  cancelButtonTitle:@"从我的相册中选择照片"
                                  destructiveButtonTitle:@"拍照"
                                  otherButtonTitles:nil];
    [actionSheet addButtonWithTitle:@"取消"];
    actionSheet.tag = 1;
    
    [actionSheet showInView:self.view];
    
}


- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex
{
   
        if (buttonIndex == 0)
        {
            UIImagePickerController *picker = [[UIImagePickerController alloc] init];
            picker.delegate = self;
            //picker.allowsEditing = YES;
            picker.sourceType = UIImagePickerControllerSourceTypeCamera;
            picker.cameraCaptureMode = UIImagePickerControllerCameraCaptureModePhoto;
            picker.cameraDevice = UIImagePickerControllerCameraDeviceFront;
            
            
            NSArray *sourceTypes =
            [UIImagePickerController availableMediaTypesForSourceType:picker.sourceType];
            if (![sourceTypes containsObject:(NSString *)kUTTypeMovie ]){
                NSLog(@"Can't save videos");
            }
            picktype = 1;
            [self presentViewController:picker animated:YES completion:nil];
        }
        
        if (buttonIndex == 1)
        {
            UIImagePickerController *picker = [[UIImagePickerController alloc] init];
            picker.delegate = self;
            //picker.allowsEditing = YES;
            picker.sourceType = UIImagePickerControllerSourceTypeSavedPhotosAlbum;
            
            
            NSArray *sourceTypes =
            [UIImagePickerController availableMediaTypesForSourceType:picker.sourceType];
            if (![sourceTypes containsObject:(NSString *)kUTTypeMovie ]){
                NSLog(@"Can't save videos");
            }
            
            picktype = 2;
            [self presentViewController:picker animated:YES completion:nil];
        }
    
}


#pragma mark video delgate

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info
{
    NSString *mediaType = [info objectForKey:UIImagePickerControllerMediaType];
    if ([mediaType isEqualToString:(NSString *)kUTTypeImage]){
        
        if (picker.sourceType==UIImagePickerControllerSourceTypeCamera) {
            [self pickVideoWithInfo:info type:1];
        }else{
            [self pickVideoWithInfo:info type:0];
        }
        
        
    }
    [picker dismissViewControllerAnimated:YES completion:nil];
}

- (void)pickVideoWithInfo2:(NSDictionary *)info type:(int)type{
    UIImage *image = [info valueForKey:UIImagePickerControllerOriginalImage];
    NSData *imageData = UIImageJPEGRepresentation(image, 0.5);
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    [manager POST:WEBSERVICE_UploadPhoto parameters:nil constructingBodyWithBlock:^(id<AFMultipartFormData> formData) {
        [formData appendPartWithFormData:imageData name:@"userHeadImage"];
    } success:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSLog(@"JSON: %@", responseObject);
        NSString *result = [responseObject objectForKey:@"resultDataType"];
        NSLog(@"jsonArray: %@", result);
        [_btHeader setImage:image forState:UIControlStateNormal];
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"Error: %@", error);
    }];
}




- (void)pickVideoWithInfo:(NSDictionary *)info type:(int)type
{
    
    UIImage *img = [info objectForKey:UIImagePickerControllerOriginalImage];
    UIImage *sThumbnail = [img scaleToSize:CGSizeMake(320, 320)];
    
    
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    
    //NSData* imageData = UIImagePNGRepresentation(img);
    NSData* smallData = UIImageJPEGRepresentation(sThumbnail, 1);
    NSString *savePath = [documentsDirectory stringByAppendingString:@"/userdefine.jpg"];
    [smallData writeToFile:savePath atomically:YES];
    //NSLog(@"data:%@",smallData);
    
    [_btHeader setImage:sThumbnail forState:UIControlStateNormal];
    
    if (type==1) {
        UIImageWriteToSavedPhotosAlbum (img, nil, nil, nil);
    }
    
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    NSURL *filePath = [NSURL fileURLWithPath:savePath];
    [manager POST:WEBSERVICE_UploadPhoto parameters:nil constructingBodyWithBlock:^(id<AFMultipartFormData> formData) {
        [formData appendPartWithFileURL:filePath name:@"userdefine" error:nil];
    } success:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSLog(@"Success: %@", responseObject);
        
        NSArray *resultarr = [responseObject objectForKey:@"resultList"];
        [self submit:resultarr];
       
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"Error: %@", error);
    }];
    /*
    
    

    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    NSString *string = WEBSERVICE_UploadPhoto;
    
    
    //NSURL *filePath = [NSURL fileURLWithPath:savePath];
    [manager POST:string parameters:nil constructingBodyWithBlock:^(id<AFMultipartFormData> formData) {
         [formData appendPartWithFormData:smallData name:@"userHeadImage"];
    } success:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        NSLog(@"JSON: %@", responseObject);
        
        NSString *result = [responseObject objectForKey:@"resultDataType"];
        
        NSLog(@"jsonArray: %@", result);
        
        
        
        if([result isEqualToString:@"1"]){
           
            [MBProgressHUD hideHUDForView:self.view animated:YES];
        }else{
            [MBProgressHUD hideHUDForView:self.view animated:YES];
        }
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"Error: %@", error);
    }];
    */
    /*
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    
    NSString *string = [NSString stringWithFormat:@"%@%@",WEBSERVICE_URL,WEBSERVICE_UpdateHeader];
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    NSURL *filePath = [NSURL fileURLWithPath:savePath];
    NSDictionary *parameters = @{@"userCode": [AppDelegate getAppDelegate].userCode,@"userHeadImage": @"2222"};
    [manager POST:string parameters:parameters constructingBodyWithBlock:^(id<AFMultipartFormData> formData) {
        //[formData appendPartWithFileURL:filePath name:@"userHeadImage" error:nil];
        
    } success:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSLog(@"JSON: %@", responseObject);
        
        NSString *result = [responseObject objectForKey:@"resultDataType"];
        
        NSLog(@"jsonArray: %@", result);
        
        if([result isEqualToString:@"1"]){
            _postlist = [responseObject objectForKey:@"resultList"];
        }
        [self reloadViews];
        
        [MBProgressHUD hideHUDForView:self.view animated:YES];
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"Error: %@", error);
        [MBProgressHUD hideHUDForView:self.view animated:YES];
    }];
    */
    
}

- (void)submit:(NSArray *)dic{
    
    
    NSString *string = [NSString stringWithFormat:@"%@%@%@",WEBSERVICE_URL,WEBSERVICE_UpdateHeader,[AppDelegate getAppDelegate].userCode];
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    manager.requestSerializer = [AFJSONRequestSerializer serializer];

    
    [manager POST:string parameters:dic
          success:^(AFHTTPRequestOperation *operation, id responseObject)
    {
        NSLog(@"JSON: %@", responseObject);
    }
          failure:
     ^(AFHTTPRequestOperation *operation, NSError *error) {
         NSLog(@"Error: %@", error);
     }];
    
}

#pragma mark messageview delgate

- (void)didClickRight:(NSString *)index{
    CommentSendViewController *clvc = [[CommentSendViewController alloc] initWithNibName:@"CommentSendViewController" bundle:nil];
    clvc.postGuid = index;
    [self.navigationController pushViewController:clvc animated:YES];
    
}

- (void)didClickLeft:(Post *)postData{
    ResentViewController *rsvc = [[ResentViewController alloc] initWithNibName:@"ResentViewController" bundle:nil];
    rsvc.data = postData;
    [self.navigationController pushViewController:rsvc animated:YES];
}

- (void)didClickCenter:(Post *)postData{
    CommentListViewController *rsvc = [[CommentListViewController alloc] initWithNibName:@"CommentListViewController" bundle:nil];
    rsvc.post = postData;
    [self.navigationController pushViewController:rsvc animated:YES];
}

@end
