//
//  MoreMailViewController.h
//  xiucherennew
//
//  Created by harry on 13-11-12.
//  Copyright (c) 2013年 harry. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MoreMailViewController : UIViewController<UIAlertViewDelegate>
@property (weak, nonatomic) IBOutlet UITextField *txtMail;

- (IBAction)clickBack:(id)sender;
- (IBAction)clickUpdate:(id)sender;
@end
