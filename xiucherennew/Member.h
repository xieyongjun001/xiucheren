//
//  Member.h
//  xiucherennew
//
//  Created by harry on 13-11-16.
//  Copyright (c) 2013年 harry. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Member : NSObject
@property (nonatomic,strong)NSString *memberid;
@property (nonatomic,strong)NSString *nickname;
@property (nonatomic,strong)NSString *headerimg;
@property (nonatomic,strong)NSString *age;
@property (nonatomic,strong)NSString *birthyear;
@property (nonatomic,strong)NSString *gender;
@property (nonatomic,strong)NSString *mobile;
@property (nonatomic,strong)NSString *mail;
@property (nonatomic,strong)NSString *stationname;
@property (nonatomic,strong)NSString *stationcode;

-(id)initWithDic:(NSDictionary *)dic;
@end
